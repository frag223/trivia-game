# Simple Trivia Game #
Simple trivia game, messing around with Redux, React

### Usage
This is for fun

### Install local
Install 
```bash

npm install 

```

### Test

```bash
npm run test
```

### running dev server

```bash
npm run start
```

## compile for zip
```bash
npm run build
```

### TODO 
1. create pipeline to deploy to prod

# Credit
https://opentdb.com/