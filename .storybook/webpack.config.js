const CaseSensitivePathsPlugin = require('case-sensitive-paths-webpack-plugin');


module.exports = ({ config }) => {

  // add our custom loaders - or anything else that's needed
  config.plugins.push(new CaseSensitivePathsPlugin());
  console.log(config);

  return config;
};