const HtmlWebpackPlugin = require('html-webpack-plugin');
require('webpack');
const path = require('path');

// application source path and build path
const APP_DIR = path.resolve(__dirname, '..', 'src', 'app');
const DIST_DIR = path.resolve(__dirname, '..', 'dist');

module.exports = {
  context: APP_DIR,
  entry: {
    app: ['./index.js'],
  },
  output: {
    path: DIST_DIR,
    filename: '[name].[hash].bundle.js',
    chunkFilename: '[name].[hash].bundle.js',
  },
  plugins: [
    new HtmlWebpackPlugin({
      template: `${APP_DIR}/views/index.html`,
      inject: true,
      excludeChunks: ['error'],
    }),
  ],
  module: {
    rules: [
      {
        test: /\.js?/,
        exclude: [/node_modules/],
        use: [{
          loader: 'babel-loader',
        }],
      },
    ],
  },
  resolve: {
    extensions: ['.js', '.jsx'],
  },
};
