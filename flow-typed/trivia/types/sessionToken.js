declare module 'token' {
  declare type TokenErrorType = {
    type: string,
    error: bool,
    message: Error,
  };

  declare type TokenLoadingType = {
    type: string,
    loading: bool
  };

  declare type TokenSuccessType = {
    type: string, 
    token: string,
  }

  declare type Action =
  | TokenErrorType
  | TokenLoadingType
  | TokenSuccessType;

  declare type State = number;
  declare type GetState = () => State;
  declare type PromiseAction = Promise<Action>;
  declare type ThunkAction = (dispatch: Dispatch, getState: GetState) => any;
  declare type Dispatch = (action: Action | ThunkAction | PromiseAction) => any;


}