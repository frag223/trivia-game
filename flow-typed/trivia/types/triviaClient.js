declare module 'triviaClient' {
  declare type triviaClientType = (url: string) => Promise<any>;

}
