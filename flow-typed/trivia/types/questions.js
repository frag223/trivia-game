
declare module 'questions' {
  declare type QuestionType = {
    id: number,
    categoryId: string,
    answered: boolean,
    category: string,
    correct_answer: string,
    difficulty: string,
    question: string,
    incorrect_answers: string[],
  };
  declare type GetQuestionParametersType = {
    token: string,
    category: string
  };

  declare type QuestionErrorType = {
    type: string,
    error: bool,
    message: Error,
  };

  declare type QuestionLoadingType = {
    type: string,
    loading: bool
  };

  declare type QuestionSuccessType = {
    type: string,
    questions: QuestionType[]
  };

  declare type ClearQuestionsSuccess = {
    type: string
  };

  declare type Action =
  | QuestionErrorType
  | QuestionLoadingType
  | QuestionSuccessType
  | ClearQuestionsSuccess;

  declare type State = {
    loading: boolean,
    questions: QuestionType[]
  };
  declare type GetState = () => State;
  declare type PromiseAction = Promise<Action>;
  declare type ThunkAction = (dispatch: Dispatch, getState: GetState) => any;
  declare type Dispatch = (action: Action | ThunkAction | PromiseAction) => any;


}