declare module 'score' {
  declare type ScoreErrorType = {
    type: string,
    error: bool,
    message: Error,
  };

  declare type ScoreLoadingType = {
    type: string,
    loading: bool
  };

  declare type ScoreSuccessType = {
    type: string, 
    score: number,
  }

  declare type ScoreType = {
    score: {
      score: number
    }
  }

  declare type Action =
  | ScoreErrorType
  | ScoreLoadingType
  | ScoreSuccessType;

  declare type State = ScoreType;
  declare type GetState = () => State;
  declare type PromiseAction = Promise<Action>;
  declare type ThunkAction = (dispatch: Dispatch, getState: GetState) => any;
  declare type Dispatch = (action: Action | ThunkAction | PromiseAction) => any;


}