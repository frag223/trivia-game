declare module 'history' {
  declare type HistoryQuestionType = {
    id: number,
    categoryId: string,
    answered: boolean,
    correct: boolean,
    category: string,
    correct_answer: string,
    difficulty: string,
    question: string,
    incorrect_answers: string[],
  };
  declare type HistoryErrorType = {
    type: string,
    error: bool,
    message: Error,
  };

  declare type HistoryLoadingType = {
    type: string,
    loading: bool
  };

  declare type HistorySuccessType = {
    type: string,
    history: HistoryQuestionType[],
  }

  declare type HistoryStateType = {

  }

  declare type Action =
  | HistoryErrorType
  | HistoryLoadingType
  | HistorySuccessType;

  declare type State = {
    loading: boolean,
    history: HistoryQuestionType[]
  };
  declare type GetState = () => State;
  declare type PromiseAction = Promise<Action>;
  declare type ThunkAction = (dispatch: Dispatch, getState: GetState) => any;
  declare type Dispatch = (action: Action | ThunkAction | PromiseAction) => any;


}