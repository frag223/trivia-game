import React from 'react';

import { storiesOf } from '@storybook/react';
import { action } from '@storybook/addon-actions';
import { linkTo } from '@storybook/addon-links';
import { Wrapper } from '../decorators/wrappers';

import CategoriesSection from '../../src/app/components/categoriesSection/CategoriesSection';

const params = {
  backgrounds: [
    { name: 'twitter', value: '#00aced', default: true },
    { name: 'facebook', value: '#3b5998' },
    { name: 'white', value: 'white' },
    { name: 'slate', value: '#323232' },
  ],
};

storiesOf('CategoriesSection', module)
  .addParameters(params)
  .addDecorator(Wrapper)
  .add('Basic setup', () => (
    <CategoriesSection
      handlePreviousClick={action('click')}
      handleNextClick={action('click')}
    >
      <a href="/">General Knowledge</a>
      <a href="/">Manga</a>
      <a href="/">Sports</a>
      <a href="/">Books</a>
      <a href="/">Music</a>
    </CategoriesSection>
  ));

