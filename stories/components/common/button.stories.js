import React from 'react';

import { storiesOf } from '@storybook/react';
import { withKnobs, number, text } from '@storybook/addon-knobs';
import { action } from '@storybook/addon-actions';
import { linkTo } from '@storybook/addon-links';

import Button from '../../../src/app/components/common/button/Button';
import RoundButton from '../../../src/app/components/common/button/RoundButton';
import ArrowButton from '../../../src/app/components/common/button/ArrowButton';


const params = {
  backgrounds: [
    { name: 'twitter', value: '#00aced', default: true },
    { name: 'facebook', value: '#3b5998' },
    { name: 'white', value: 'white' },
    { name: 'slate', value: '#323232' },
  ],
};

storiesOf('Button', module)
  .addParameters(params)
  .add('All Standard Buttons', () => (
    <div>
      <Button primary handleClick={action('clicked')}>Hello Button</Button>
      <Button primary info handleClick={action('clicked')}>Hello Button</Button>
      <Button primary danger handleClick={action('clicked')}>Hello Button</Button>
      <Button primary success handleClick={action('clicked')}>Hello Button</Button>
      <Button info handleClick={action('clicked')}>Hello Button</Button>
      <Button danger handleClick={action('clicked')}>Hello Button</Button>
      <Button success handleClick={action('clicked')}>Hello Button</Button>
      <Button handleClick={action('clicked')}>Hello Button</Button>
    </div>
  ))
  .add('All Round Buttons', () => (
    <div>
      <RoundButton primary handleClick={action('clicked')}>btn</RoundButton>
      <RoundButton primary info handleClick={action('clicked')}>INFO</RoundButton>
      <RoundButton primary danger handleClick={action('clicked')}>NO</RoundButton>
      <RoundButton primary success handleClick={action('clicked')}>YES</RoundButton>
      <RoundButton info handleClick={action('clicked')}>btn</RoundButton>
      <RoundButton danger handleClick={action('clicked')}>btn</RoundButton>
      <RoundButton success handleClick={action('clicked')}>YES</RoundButton>
      <RoundButton handleClick={action('clicked')}>BTN</RoundButton>
    </div>
  ))
  .add('All Arrow Buttons', () => (
    <div>
      <ArrowButton key="hello" left primary handleClick={action('clicked')} />
      <ArrowButton left primary info handleClick={action('clicked')} />
      <ArrowButton left primary danger handleClick={action('clicked')} />
      <ArrowButton left primary success handleClick={action('clicked')} />
      <ArrowButton info handleClick={action('clicked')} />
      <ArrowButton danger handleClick={action('clicked')} />
      <ArrowButton success handleClick={action('clicked')} />
      <ArrowButton handleClick={action('clicked')} />
    </div>
  ));
