import React from 'react';

import { storiesOf } from '@storybook/react';
import { withKnobs, number, text } from '@storybook/addon-knobs';
import { action } from '@storybook/addon-actions';
import { linkTo } from '@storybook/addon-links';
import { Wrapper } from '../../decorators/wrappers';

import LeftArrow from '../../../src/app/components/common/Arrows/LeftArrow';
import RightArrow from '../../../src/app/components/common/Arrows/RightArrow';

const params = {
  backgrounds: [
    { name: 'twitter', value: '#00aced', default: true },
    { name: 'facebook', value: '#3b5998' },
    { name: 'white', value: 'white' },
    { name: 'slate', value: '#323232' },
  ],
};

storiesOf('Arrows', module)
  .addParameters(params)
  .addDecorator(withKnobs)
  .addDecorator(Wrapper)
  .add('Left Arrow', () => {
    const height = number('Height', 40);
    const width = number('Width', 40);
    const fill = text('Colour Fill', 'white');

    return (
      <div>
        <LeftArrow height={height} width={width} fill={fill} />
      </div>
    );
  })
  .add('Right Arrow', () => {
    const height = number('Height', 40);
    const width = number('Width', 40);
    const fill = text('Colour Fill', 'white');

    return (
      <div>
        <RightArrow height={height} width={width} fill={fill} />
      </div>
    );
  });
