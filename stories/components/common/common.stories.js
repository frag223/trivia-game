import React from 'react';

import { storiesOf } from '@storybook/react';
import { withKnobs, number, text } from '@storybook/addon-knobs';
import { action } from '@storybook/addon-actions';
import { linkTo } from '@storybook/addon-links';
import { Wrapper } from '../../decorators/wrappers';

import Button from '../../../src/app/components/common/button/Button';
import LoaderSvg from '../../../src/app/components/common/loader/LoadingSvg';
import LoaderCard from '../../../src/app/components/common/loader/LoadingCard';
import Card from '../../../src/app/components/common/card/Card';
import PercentageMeter from '../../../src/app/components/common/percentageMeter/PercentageMeter';
import List from '../../../src/app/components/common/List';
import Title from '../../../src/app/components/common/Title';

const params = {
  backgrounds: [
    { name: 'twitter', value: '#00aced', default: true },
    { name: 'facebook', value: '#3b5998' },
    { name: 'white', value: 'white' },
    { name: 'slate', value: '#323232' },
  ],
};


storiesOf('Title', module)
  .add('Main Title', () => (
    <div>
      <Title type="main">A Title</Title>
    </div>
  ))
  .add('Subtitle', () => (
    <div>
      <Title type="subTitle">A Title</Title>
    </div>
  ));

storiesOf('Loader', module)
  .addParameters(params)
  .addDecorator(Wrapper)
  .add('Loading Svg', () => (
    <LoaderSvg />
  ))
  .add('Loading Card', () => (
    <LoaderCard />
  ));


storiesOf('Card', module)
  .addParameters(params)
  .addDecorator(Wrapper)
  .add('Card with paragraph', () => (
    <Card title="Card with paragraph">
      <p style={{ width: '90%' }}>Lorem ipsum dolor sit amet,
        consectetur adipiscing elit.
        Phasellus quis blandit diam,
        et sollicitudin dolor.
        Curabitur lorem elit,
        hendrerit vitae molestie vitae,
        fringilla non lacus.
        Nam hendrerit congue orci et consectetur.
        Aenean eu euismod urna.
        Praesent aliquet ligula vitae libero congue,
        in dictum purus malesuada.
        Proin accumsan in mauris in pharetra.
        Sed auctor ante ipsum,
        id congue diam rutrum at.</p>
    </Card>
  ))
  .add('Card with List', () => (
    <Card title="Card with List">
      <List>
        <Button handleClick={action('clicked')}>HOME</Button>
        <Button handleClick={action('clicked')}>PROFILE</Button>
        <Button handleClick={action('clicked')}>ABOUT</Button>
      </List>
    </Card>
  ));

storiesOf('PercentageMeter', module)
  .addParameters(params)
  .addDecorator(Wrapper)
  .add('PercentageMeter ALL', () => (
    <div>
      <PercentageMeter
        percentage="20"
        size="sm"
        colour="pink"
      />
      <PercentageMeter
        percentage="40"
        size="md"
        colour="purple"
      />
      <PercentageMeter
        percentage="60"
        size="lg"
        colour="coral"
      />
      <PercentageMeter
        percentage="80"
        size="xl"
        colour="blue"
      />
    </div>
  ))
  .add('PercentageMeter in a Card', () => (
    <Card title="Profile">
      <p>General</p>
      <PercentageMeter
        percentage="80"
        size="md"
        colour="coral"
      />
    </Card>));

