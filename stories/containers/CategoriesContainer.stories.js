import React from 'react';

import { storiesOf } from '@storybook/react';
import { action } from '@storybook/addon-actions';
import { linkTo } from '@storybook/addon-links';
import { withKnobs, boolean } from '@storybook/addon-knobs';

import { ProviderWrapper } from '../decorators/wrappers';
import { categoriesLoading, categoriesSuccess } from '../../src/app/actions/categories';

import configStore from '../../src/app/store/configureStore';

import CategoriesContainer from '../../src/app/containers/CategoriesContainer';


const store = configStore();
const categoriesProps = [
  { id: 1, name: 'General Knowledge' },
  { id: 2, name: 'Anime' },
  { id: 3, name: 'Books' },
  { id: 4, name: 'General' },
  { id: 5, name: 'Sports' },
  { id: 6, name: 'Gaming' },
  { id: 7, name: 'film' },
  { id: 8, name: 'Theatres' },
  { id: 9, name: 'Computers' },
  { id: 10, name: 'TV' },
];

const withProvider = story => (
  <ProviderWrapper store={store}>
    { story() }
  </ProviderWrapper>
);

const params = {
  backgrounds: [
    { name: 'twitter', value: '#00aced', default: true },
    { name: 'facebook', value: '#3b5998' },
    { name: 'white', value: 'white' },
    { name: 'slate', value: '#323232' },
  ],
};

storiesOf('CategoriesContainer', module)
  .addParameters(params)
  .addDecorator(withKnobs)
  .addDecorator(withProvider)
  .add('Basic', () => {
    const loadingState = boolean('Loading', false, 'CategoryContainerLoading');
    store.dispatch(categoriesSuccess(categoriesProps));
    if (loadingState) {
      store.dispatch(categoriesLoading(true));
    } else {
      store.dispatch(categoriesLoading(false));
    }

    return (
      <CategoriesContainer />
    );
  });
