import React from 'react';

import { storiesOf } from '@storybook/react';
import { action } from '@storybook/addon-actions';
import { linkTo } from '@storybook/addon-links';
import { withKnobs, boolean } from '@storybook/addon-knobs';

import { ProviderWrapper } from '../decorators/wrappers';
import { updateQuestionsList, addQuestionsLoading } from '../../src/app/actions/questions';

import configStore from '../../src/app/store/configureStore';

import QuestionsContainer from '../../src/app/containers/QuestionsContainer';


const store = configStore();
const categoriesProps = [
  { id: 1,
    categoryId: 11,
    answered: false,
    answers: ['yeah', 'naa', 'baz', 'hello'],
    category: 'general knowledge',
    correct_answer: 'hello',
    difficulty: 'easy',
    question: 'what day is it',
    incorrect_answers: ['yeah', 'naa', 'baz'] },
];

const withProvider = story => (
  <ProviderWrapper store={store}>
    { story() }
  </ProviderWrapper>
);

const params = {
  backgrounds: [
    { name: 'twitter', value: '#00aced', default: true },
    { name: 'facebook', value: '#3b5998' },
    { name: 'white', value: 'white' },
    { name: 'slate', value: '#323232' },
  ],
};

storiesOf('QuestionsContainer', module)
  .addParameters(params)
  .addDecorator(withKnobs)
  .addDecorator(withProvider)
  .add('Basic', () => {
    const loadingState = boolean('Loading', false, 'CategoryContainerLoading');
    store.dispatch(updateQuestionsList(categoriesProps));
    if (loadingState) {
      store.dispatch(addQuestionsLoading(true));
    } else {
      store.dispatch(addQuestionsLoading(false));
    }

    return (
      <QuestionsContainer />
    );
  });
