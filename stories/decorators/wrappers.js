import React from 'react';
import { BrowserRouter as Router } from 'react-router-dom';
import { Provider } from 'react-redux';

const styles = {
  display: 'flex',
  justifyContent: 'center',
};
const Wrapper = storyFn => (<div style={styles}>{storyFn()}</div>);


const ProviderWrapper = ({ children, store }) => (
  <Provider store={store}>
    <Router>
      { children }
    </Router>
  </Provider>
);

export default ProviderWrapper;


export { Wrapper, ProviderWrapper };
