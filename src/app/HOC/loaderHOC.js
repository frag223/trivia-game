import React, { Component } from 'react';
import Loader from '../components/common/loader/LoadingCard';

const LoaderHOC = propName => WrappedComponent => class LoaderHOC extends Component {
  isLoading() {
    return (
      this.props[propName].loading === undefined ||
        this.props[propName].loading === null ||
        this.props[propName].loading
    );
  }
  render() {
    return this.isLoading() ?
      <Loader /> :
      <WrappedComponent {...this.props} />;
  }
};

export default LoaderHOC;
