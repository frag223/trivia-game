import React, { Component } from 'react';
import ErrorBoundarySection from '../components/ErrorBoundarySection';

const ErrorBoundary = () => WrappedComponent => class ErrorBoundary extends Component {
    state = {
      hasError: false,
    };
    componentDidCatch(error, info) {
      this.setState({ hasError: true });
    }

    render() {
      return this.state.hasError ?
        <ErrorBoundarySection /> :
        <WrappedComponent {...this.props} />;
    }
};

export default ErrorBoundary;
