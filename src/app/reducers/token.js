import {
  REQUEST_TOKEN_LOADING,
  REQUEST_TOKEN_ERROR,
  REQUEST_TOKEN_SUCCESS,
} from '../config/constants';

export default function (state = {}, action) {
  switch (action.type) {
    case REQUEST_TOKEN_LOADING:
      return {
        ...state,
        loading: action.loading,
      };

    case REQUEST_TOKEN_ERROR:
      return {
        error: action.error,
        message: action.message,
      };
    case REQUEST_TOKEN_SUCCESS:
      return {
        ...state,
        token: action.token,
      };
    default:
      return state;
  }
}

