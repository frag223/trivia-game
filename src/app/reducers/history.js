/* @flow */

import {
  ADD_HISTORY_LOADING,
  ADD_HISTORY_ERROR,
  ADD_HISTORY_SUCCESS,
  CHART_COLOUR_SCALE,
} from '../config/constants';

import type {
  HistoryQuestionType,
  State,
} from 'history';


import { randomNumber, unique, immutablePush } from '../lib/immutable';

declare type ChartDataType = {
  id: number,
  category: string,
  questions: HistoryQuestionType[],
  correct: number,
};

const initState = {
  loading: true,
  history: [],
};

export default function (state: State = initState, action: any) {
  switch (action.type) {
    case ADD_HISTORY_ERROR:
      return {
        error: action.error,
        message: action.message,
      };
    case ADD_HISTORY_LOADING:
      return {
        ...state,
        loading: action.loading,
      };
    case ADD_HISTORY_SUCCESS:
      return {
        ...state,
        history: action.history,
      };
    default:
      return state;
  }
}


export const getHistoryLength = (history: HistoryQuestionType[]): number => history.length;

export const historyMerge = (history: HistoryQuestionType[], question: HistoryQuestionType): HistoryQuestionType[] => {
  const newHistory = [].concat(history);
  return newHistory.concat(question);
};

export const filterByCategory = (history: HistoryQuestionType[], category: string): HistoryQuestionType[] => history.filter(question => question.category === category);

export const getCorrectTotal = (history: HistoryQuestionType[]): number => {
  const correct = history.filter(question => question.correct === true);
  return correct.length;
};

export const getAllCategories = (history: HistoryQuestionType[]): string[] => {
  let categories: string[] = [];
  history.forEach((question) => {
    categories = immutablePush(categories, question.category);
  });
  return unique(categories);
};

export const transformData = (history: HistoryQuestionType[]): ChartDataType[] => {
  const categories: string[] = getAllCategories(history);
  return categories.map((category) => {
    const questionsForCategory = filterByCategory(history, category);
    const correctlyAnsweredTotal = getCorrectTotal(questionsForCategory);
    return {
      id: randomNumber(1, 1000),
      category,
      questions: questionsForCategory,
      correct: correctlyAnsweredTotal,
    };
  });
};

export const generateChartData = (history: State, chartSize: string) => {
  if (history.loading === false) {
    const transformedData = transformData(history.history);
    return transformedData.map((data, index) => ({
      category: data.category,
      percent: (Math.round((data.correct / data.questions.length) * 100)),
      size: chartSize,
      colour: CHART_COLOUR_SCALE[index],
    }));
  }
  return [];
};
