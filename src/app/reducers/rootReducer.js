import { combineReducers } from 'redux';
import categories from './categories';
import score from './score';
import token from './token';
import questions from './questions';
import history from './history';

export default combineReducers({
  categories,
  score,
  token,
  questions,
  history,
  // More reducers if there are
  // can go here
});
