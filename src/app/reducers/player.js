import {
  UPDATE_PLAYER_ERROR,
  UPDATE_PLAYER_LOADING,
  UPDATE_PLAYER_SUCCESS,
} from '../config/constants';

export default function (state = null, action) {
  switch (action.type) {
    case UPDATE_PLAYER_ERROR:
      return {
        error: action.error,
        message: action.message,
      };
    case UPDATE_PLAYER_LOADING:
      return {
        ...state,
        loading: action.loading,
      };
    case UPDATE_PLAYER_SUCCESS:
      return action.player;
    default:
      return state;
  }
}
