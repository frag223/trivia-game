import {
  PLAYER_FORM,
} from '../config/constants';

const playerForm = (state = [], action) => {
  switch (action.type) {
    case PLAYER_FORM:
      return [
        ...state,
        Object.assign(action.form),
      ];
    default:
      return state;
  }
};

export default playerForm;
