
import {
  ADD_CATEGORIES_SUCCESS,
  ADD_CATEGORIES_ERROR,
  ADD_CATEGORIES_LOADING,
} from '../config/constants';

import { chunkArray } from '../lib/immutable';

export default function categories(state = {}, action) {
  switch (action.type) {
    case ADD_CATEGORIES_ERROR:
      return {
        error: action.error,
        message: action.message,
      };
    case ADD_CATEGORIES_LOADING:
      return {
        ...state,
        loading: action.loading,
      };
    case ADD_CATEGORIES_SUCCESS:
      return {
        ...state,
        categories: action.categories,
      };
    default:
      return state;
  }
}

/**
 * Chunks an array into nested arrays with a length of 5
 */
export function paginateCategories(state) {
  if (state.loading === false) {
    return chunkArray(state.categories, 5);
  }
  return [];
}
