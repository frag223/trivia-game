/** @flow */

import type {
  QuestionErrorType,
  QuestionLoadingType,
  QuestionSuccessType,
  ClearQuestionsSuccess,
  GetQuestionParametersType,
  Action,
  State,
  GetState,
  PromiseAction,
  ThunkAction,
  Dispatch,
  QuestionType,
} from 'questions';

import type { HistoryQuestionType } from 'history';

import {
  ADD_QUESTIONS_ERROR,
  ADD_QUESTIONS_LOADING,
  ADD_QUESTIONS_SUCCESS,
  UPDATE_QUESTION_SUCCESS,
  CLEAR_QUESTIONS_SUCCESS,
} from '../config/constants';

export const filterQuestionsById = (questions: QuestionType[], id: number): QuestionType => {
  const question = questions.filter(question => question.id === id);
  return question[0];
};

export const findIndexById = (questions: QuestionType[], id: number): number => questions.findIndex(question => question.id === id);

export const removeQuestionById = (questions: QuestionType[], id: number): QuestionType[] => {
  const array: QuestionType[] = [].concat(questions);
  const questionIndex = findIndexById(questions, id);
  array.splice(questionIndex, 1);
  return array;
};

export const verifyAnswerIsCorrect = (questions: QuestionType[], id: number, answer: string): boolean => {
  const question = filterQuestionsById(questions, id);
  return question.correct_answer.includes(answer);
};

export const addQuestionProperties = (question: QuestionType, isCorrect: boolean, answer: string): HistoryQuestionType => Object.assign({}, question, {
  answered: true,
  submitedAnswer: answer,
  correct: isCorrect,
});

export const updateQuestionProperties = (question: QuestionType, answer: string) => {
  const isCorrect = question.correct_answer.includes(answer);
  return addQuestionProperties(question, isCorrect, answer);
};

export const unshiftQuestionList = (question: QuestionType, questionList: QuestionType[]): QuestionType[] => [].concat(question, questionList);

export const getCurrentQuestion = (questions) => {
  if (questions.loading === false) {
    return questions.questions[0];
  }
  return {};
};

export default function questions(state: any = {}, action: any): any {
  switch (action.type) {
    case ADD_QUESTIONS_ERROR:
      return {
        error: action.error,
        message: action.message,
      };
    case ADD_QUESTIONS_LOADING:
      return {
        ...state,
        loading: action.loading,
      };
    case ADD_QUESTIONS_SUCCESS:
      return {
        ...state,
        questions: action.questions,
      };
    case UPDATE_QUESTION_SUCCESS:
      return {
        ...state,
        questions: action.questions,
      };
    case CLEAR_QUESTIONS_SUCCESS:
      return {};
    default:
      return state;
  }
}
