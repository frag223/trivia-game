import {
  UPDATE_SCORE_LOADING,
  UPDATE_SCORE_SUCCESS,
  UPDATE_SCORE_ERROR,
} from '../config/constants';

export default function (state = { score: 0 }, action) {
  switch (action.type) {
    case UPDATE_SCORE_ERROR:
      return {
        error: action.error,
        message: action.message,
      };
    case UPDATE_SCORE_LOADING:
      return {
        ...state,
        loading: action.loading,
      };
    case UPDATE_SCORE_SUCCESS:
      return {
        ...state,
        score: action.score,
      };
    default:
      return state;
  }
}

