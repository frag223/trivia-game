import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';

// actions
import { updateQuestionsList, updateQuestionAction } from '../actions/questions';
import updateScore from '../actions/score';
import { addToHistory } from '../actions/history';
// selectors
import {
  getCurrentQuestion,
  removeQuestionById,
  updateQuestionProperties,
} from '../reducers/questions';

// containers
import AnswersContainer from './AnswersContainer';
// components
import QuestionSection from '../components/QuestionsSection/QuestionSection';
import QuestionAction from '../components/QuestionsSection/QuestionsActions';
import CompleteSection from '../components/CompleteSection';
import LoaderHOC from '../HOC/loaderHOC';


class QuestionsContainer extends Component {
  static propTypes = {
    currentQuestion: PropTypes.shape({
      id: PropTypes.number,
      categoryId: PropTypes.number,
      answered: PropTypes.bool,
      correct: PropTypes.bool,
      answers: PropTypes.arrayOf(PropTypes.string),
      category: PropTypes.string,
    }),
    questions: PropTypes.shape({
      loading: PropTypes.bool.isRequired,
      questions: PropTypes.array.isRequired,
    }),
    updateScore: PropTypes.func.isRequired,
    addToHistory: PropTypes.func.isRequired,
    updateQuestionAction: PropTypes.func.isRequired,
    updateQuestionsList: PropTypes.func.isRequired,
  }
  static defaultProps = {
    currentQuestion: {},
    questions: {
      loading: true,
      questions: [],
    },
  }
  handleAnswerSubmit = this.handleAnswerSubmit.bind(this);
  handleNextQuestion = this.handleNextQuestion.bind(this);

  updateScore() {
    return this.props.currentQuestion.correct ?
      this.props.updateScore() : '';
  }

  updateQuestionList(id, answer) {
    const { currentQuestion } = this.props;
    const updatedQuestion = updateQuestionProperties(currentQuestion, answer);
    this.props.addToHistory(updatedQuestion);
    this.props.updateQuestionAction(currentQuestion, answer);
  }

  removeQuestionFromList(id) {
    const questions = this.props.questions.questions;
    const updatedQuestions = removeQuestionById(questions, id);
    this.props.updateQuestionsList(updatedQuestions);
  }

  handleAnswerSubmit(answer, id, event) {
    event.preventDefault();
    this.updateScore();
    this.updateQuestionList(id, answer);
  }

  handleNextQuestion(id, event) {
    event.preventDefault();
    this.removeQuestionFromList(id);
  }

  render() {
    const { questions, currentQuestion } = this.props;
    if (questions.questions.length === 0) {
      return <CompleteSection />;
    }
    return (
      <QuestionSection
        id={currentQuestion.id}
        question={currentQuestion.question}
      >
        <AnswersContainer
          handleAnswerSubmit={this.handleAnswerSubmit}
          {...currentQuestion}
        />
        <QuestionAction
          hidden={currentQuestion.answered}
          id={currentQuestion.id}
          handleNextQuestion={this.handleNextQuestion}
        />
      </QuestionSection>
    );
  }
}


const mapStateToProps = (state) => {
  const questions = state.questions;
  const currentQuestion = getCurrentQuestion(questions);
  return {
    questions,
    currentQuestion,
    history: state.history,
  };
};

const mapDispatchToProps = {
  updateQuestionsList,
  updateScore,
  addToHistory,
  updateQuestionAction,
};


export default connect(
  mapStateToProps,
  mapDispatchToProps,
)(LoaderHOC('questions')(QuestionsContainer));
