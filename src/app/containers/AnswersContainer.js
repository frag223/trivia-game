import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';

// components
import AnswersSection from '../components/answersSection/AnswersSection';
import Button from '../components/common/button/Button';


class AnswersContainer extends Component {
  static propTypes = {
    answers: PropTypes.arrayOf(PropTypes.string).isRequired,
    handleAnswerSubmit: PropTypes.func.isRequired,
    id: PropTypes.number.isRequired,
    correct_answer: PropTypes.string.isRequired,
    answered: PropTypes.bool,
  }

  static defaultProps = {
    answered: false,
  }

  state = {
    selected: null,
  }

  handleStateSelection = this.handleStateSelection.bind(this);

  handleStateSelection(answer, id, event) {
    this.setState({ selected: answer });
    this.props.handleAnswerSubmit(answer, id, event);
  }

  render() {
    const { answered, id } = this.props;
    if (answered) {
      return [
        <AnswersSection key="answer-section-answered">
          {
            this.props.answers.map(answer => (
              <Button
                key={`answer-button-answered-${answer}`}
                primary={(answer === this.state.selected || answer === this.props.correct_answer)}
                danger={(answer === this.state.selected && answer !== this.props.correct_answer)}
                success={(this.props.correct_answer === answer)}
                handleClick={() => { }}
              >
                {answer}
              </Button>
            ))
          }
        </AnswersSection>,
      ];
    }
    return (
      <AnswersSection>
        {
          this.props.answers.map(answer => (
            <Button
              key={`answer-button-${answer}`}
              handleClick={event => this.handleStateSelection(answer, id, event)}
            >
              {answer}
            </Button>
          ))
        }
      </AnswersSection>
    );
  }
}

const mapStateToProps = () => ({
});

const mapDispatchToProps = {
};

export default connect(
  mapStateToProps,
  mapDispatchToProps,
)(AnswersContainer);
