import React from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';

// actions

// selectors
import { getHistoryLength } from '../reducers/history';

// components
import NotificationSection from '../components/notificationSection/NotificationSection';


const ScoreContainer = ({ score, questionsAnswered }) => (
  <NotificationSection>
    {`${score} / ${questionsAnswered}`}
  </NotificationSection>
);

ScoreContainer.propTypes = {
  score: PropTypes.number.isRequired,
  questionsAnswered: PropTypes.number.isRequired,
};

const mapStateToProps = state => ({
  score: state.score.score,
  questionsAnswered: getHistoryLength(state.history.history),
});

export default connect(
  mapStateToProps,
)(ScoreContainer);
