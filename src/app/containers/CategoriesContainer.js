import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import LinkButton from '../components/common/button/LinkButton';


// selectors
import { paginateCategories } from '../reducers/categories';

// components
import loaderHOC from '../HOC/loaderHOC';
import CategoriesSection from '../components/categoriesSection/CategoriesSection';

class CategoriesContainer extends Component {
  static propTypes = {
    totalCategoryPages: PropTypes.number,
    categoriesPaginated: PropTypes.arrayOf(PropTypes.array),
  }
  static defaultProps = {
    totalCategoryPages: 0,
    categoriesPaginated: [],
  }

  state = {
    current: 0,
  };

  handlePreviousClick = this.handlePreviousClick.bind(this);
  handleNextClick = this.handleNextClick.bind(this);

  handlePreviousClick(event) {
    event.preventDefault();
    const { current } = this.state;
    const total = this.props.totalCategoryPages;
    this.setState({
      current: current - 1 === -1 ? total - 1 : current - 1,
    });
  }

  handleNextClick(event) {
    event.preventDefault();
    const { current } = this.state;
    const total = this.props.totalCategoryPages;
    this.setState({
      current: current + 1 === total ? 0 : current + 1,
    });
  }

  render() {
    const categories = this.props.categoriesPaginated[this.state.current];
    return (
      <CategoriesSection
        categories={categories}
        handlePreviousClick={this.handlePreviousClick}
        handleNextClick={this.handleNextClick}
      >
        {
          categories.map(item => (
            <LinkButton
              key={`category-button-${item.id}`}
              to={`/questions/${item.id}`}
            >
              {item.name}
            </LinkButton>
          ))
        }
      </CategoriesSection>
    );
  }
}


const mapStateToProps = (state) => {
  const categories = state.categories;
  const categoriesPaginated = paginateCategories(categories);
  const totalCategoryPages = categoriesPaginated.length;
  return {
    categories,
    categoriesPaginated,
    totalCategoryPages,
  };
};


export default connect(
  mapStateToProps,
)(loaderHOC('categories')(CategoriesContainer));
