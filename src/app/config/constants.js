export const ADD_CATEGORIES_LOADING = 'ADD_CATEGORIES_LOADING';
export const ADD_CATEGORIES_ERROR = 'ADD_CATEGORIES_ERROR';
export const ADD_CATEGORIES_SUCCESS = 'ADD_CATEGORIES_SUCCESS';
export const ADD_HISTORY_LOADING = 'ADD_HISTORY_LOADING';
export const ADD_HISTORY_ERROR = 'ADD_HISTORY_ERROR';
export const ADD_HISTORY_SUCCESS = 'ADD_HISTORY_SUCCESS';
export const UPDATE_SCORE_LOADING = 'UPDATE_SCORE_LOADING';
export const UPDATE_SCORE_SUCCESS = 'UPDATE_SCORE_SUCCESS';
export const UPDATE_SCORE_ERROR = 'UPDATE_SCORE_ERROR';
export const UPDATE_PLAYER = 'UPDATE_PLAYER';
export const UPDATE_PLAYER_SUCCESS = 'UPDATE_PLAYER_SUCCESS';
export const UPDATE_PLAYER_ERROR = 'UPDATE_PLAYER_ERROR';
export const UPDATE_PLAYER_LOADING = 'UPDATE_PLAYER_LOADING';
export const PLAYER_FORM = 'PLAYER_FORM';
export const PLAYER_FORM_ERROR = 'PLAYER_FORM_ERROR';
export const PLAYER_FORM_SUCCESS = 'PLAYER_FORM_SUCCESS';
export const REQUEST_TOKEN_LOADING = 'REQUEST_TOKEN_LOADING';
export const REQUEST_TOKEN_ERROR = 'REQUEST_TOKEN_ERROR';
export const REQUEST_TOKEN_SUCCESS = 'REQUEST_TOKEN_SUCCESS';
export const ADD_QUESTIONS_LOADING = 'ADD_QUESTIONS_LOADING';
export const ADD_QUESTIONS_SUCCESS = 'ADD_QUESTIONS_SUCCESS';
export const ADD_QUESTIONS_ERROR = 'ADD_QUESTIONS_ERROR';
export const CLEAR_QUESTIONS_SUCCESS = 'CLEAR_QUESTIONS_SUCCESS';
export const UPDATE_QUESTION_LOADING = 'UPDATE_QUESTION_LOADING';
export const UPDATE_QUESTION_ERROR = 'UPDATE_QUESTION_ERROR';
export const UPDATE_QUESTION_SUCCESS = 'UPDATE_QUESTION_SUCCESS';
export const UPDATE_CATEGORY_QUESTIONS = 'UPDATE_CATEGORY_QUESTIONS';
export const CLEAR_CATEGORY_QUESTIONS = 'CLEAR_CATEGORY_QUESTIONS';
export const TRIVIA_URL = 'https://opentdb.com';
export const MAX_QUESTIONS_COUNT = 10;
export const CHART_COLOUR_SCALE = ['tomato', 'orange', 'gold', 'cyan', 'navy', 'chartreuse', 'crimson', 'darkmagenta'];
