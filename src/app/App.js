import React from 'react';
import { Provider } from 'react-redux';
import PropTypes from 'prop-types';
import {
  BrowserRouter as Router,
  Route,
} from 'react-router-dom';
// components
import MenuSection from './components/menuSection/MenuSection';
// Views
import ProfileScreen from './views/ProfileScreen';
import MainScreen from './views/MainScreen';
import CategoriesScreen from './views/CategoriesScreen';
import QuestionsScreen from './views/QuestionsScreen';

// css
import './app.css';

const RoutesContainer = (props) => {
  const { reduxStore } = props;
  return (
    <Provider store={reduxStore}>
      <Router>
        <div>
          <MenuSection />
          <Route exact path="/" component={MainScreen} />
          <Route exact path="/new-game" component={CategoriesScreen} />
          <Route exact path="/categories" component={CategoriesScreen} />
          <Route exact path="/profile" component={ProfileScreen} />
          <Route path="/questions/:id" component={QuestionsScreen} />
        </div>
      </Router>
    </Provider>
  );
};

RoutesContainer.propTypes = {
  reduxStore: PropTypes.object.isRequired,
};

export default RoutesContainer;
