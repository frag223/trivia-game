

const errorMessage = (params) => {
  const responsecode = params.response_code;
  const responseMessage = params.response_message;
  const stringMessage = JSON.stringify({
    response_code: responsecode,
    response_message: responseMessage,
  });
  return stringMessage;
};

export default function validateResponse(responsePayload) {
  if (responsePayload.response_code !== 0) {
    const message = errorMessage(responsePayload);
    throw Error(message);
  }
  return responsePayload;
}
