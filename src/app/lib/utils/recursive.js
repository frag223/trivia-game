/* @flow */

export default function recusive(fn: Function, ...rest: any[]) {
  const [head, ...tail] = rest;
  if (head === undefined && !tail.length) { return []; }
  if (tail.length) {
    if (Array.isArray(head)) {
      return [recusive(fn, ...head), ...(recusive(fn, ...tail))];
    }
    return [fn(head), ...(recusive(fn, ...tail))];
  }
  return [fn(head)];
}
