/* @flow */


export function decode(value: string): string {
  const b = Buffer.from(value, 'base64');
  return b.toString();
}

function decodeArray(arr: string[] = []): string[] {
  return arr.map(item => decode(item));
}

export function decodeObject(encodedObject: { [string]: string | string[] } = {}) {
  let results = {};
  const list = Object.entries(encodedObject);
  list.forEach(([key, value]: [string, any]) => {
    if (Array.isArray(value)) {
      results = Object.assign({}, results, { [key]: decodeArray(value) });
    } else {
      results = Object.assign({}, results, { [key]: decode(value) });
    }
  });
  return results;
}
