import { randomNumber, immutablePush } from '../lib/immutable';
import { decodeObject } from '../lib/utils/decode';
import shuffle from '../lib/shuffle';

export function transformQuestion(object) {
  const decoded = decodeObject(object);
  const allAnswers = immutablePush(
    decoded.incorrect_answers,
    decoded.correct_answer,
  );
  return {
    id: randomNumber(1, 1000000),
    answered: false,
    answers: shuffle(allAnswers),
    ...decoded,
  };
}

export function transformCategory(triviaCategoryObject) {
  if (triviaCategoryObject.name.includes(':')) {
    const [, suffix] = triviaCategoryObject.name.split(':');
    return {
      category: suffix.trim(),
      name: suffix.trim(),
      id: triviaCategoryObject.id,
    };
  }
  return {
    category: triviaCategoryObject.name,
    id: triviaCategoryObject.id,
    name: triviaCategoryObject.name,
  };
}
