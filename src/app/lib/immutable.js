/* @flow */

/**
 * Creates new array and adds a new entry
 */
export function immutablePush(arr: any[], newEntry: any): any[] {
  return [...arr, newEntry];
}

/**
 * Generates a number between the min and max
 */
export function randomNumber(min: number, max: number): number {
  return Math.floor((Math.random() * max) + min);
}

/**
 * Creates new array and returns all non duplicated string, number
 */
export function unique(array: ?string[] | ?number[]): any[] {
  return [...new Set(array)];
}

/**
 * Returns a new array with the data chunked into smaller arrays which length is determined by chunk size
 */
export function chunkArray(array: any[], chunkSize: number): any[] {
  let index = 0;
  const arrayLength = array.length;
  let tempArray = [];

  for (index; index < arrayLength; index += chunkSize) {
    const myChunk = array.slice(index, index + chunkSize);
    tempArray = immutablePush(tempArray, myChunk);
  }

  return tempArray;
}
