import React from 'react';
import PropTypes from 'prop-types';

import './notificationSection.css';


const notificationSection = (props) => {
  const { children } = props;
  return (
    <section className="notification-section shadow">
      <p>{children}</p>
    </section>
  );
};

notificationSection.propTypes = {
  children: PropTypes.node.isRequired,
};

export default notificationSection;
