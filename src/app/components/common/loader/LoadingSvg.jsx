import React from 'react';
import styled from 'styled-components';

const Div = styled.div`
  display: flex;
  width: 100%;
  height: 100%;
  align-items: center;
  justify-content: center;
`;
const Svg = styled.svg`
  display: flex;
  width: 100;
  fill: lightblue;
`;

export default function LoadingSvg() {
  return (
    <Div className="loading-container">
      <Svg className="loading-svg" width="70" height="20">
        <circle className="loading-svg-circle" cx="10" cy="10" r="0">
          <animate attributeName="r" from="0" to="10" values="0;10;10;10;0" dur="1000ms" repeatCount="indefinite" />
        </circle>
        <circle className="loading-svg-circle" cx="35" cy="10" r="0">
          <animate
            attributeName="r"
            from="0"
            to="10"
            values="0;10;10;10;0"
            begin="200ms"
            dur="1000ms"
            repeatCount="indefinite"
          />
        </circle>
        <circle className="loading-svg-circle" cx="60" cy="10" r="0">
          <animate
            attributeName="r"
            from="0"
            to="10"
            values="0;10;10;10;0"
            begin="400ms"
            dur="1000ms"
            repeatCount="indefinite"
          />
        </circle>
      </Svg>
    </Div>
  );
}

