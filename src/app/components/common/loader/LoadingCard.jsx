import React from 'react';

// components
import Card from '../card/Card';
import LoadingSvg from './LoadingSvg';
import Title from '../Title';


export default function Loading() {
  return (
    <Card>
      <Title type="subTitle">Loading...</Title>
      <LoadingSvg />
    </Card>
  );
}

