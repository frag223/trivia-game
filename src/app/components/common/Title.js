import React from 'react';
import PropTypes from 'prop-types';

const typeOfTitle = (type, children) => {
  const title = {
    main: <h1>{children}</h1>,
    subTitle: <h3>{children}</h3>,
  };
  return title[type];
};

const Title = ({ type, children }) => (typeOfTitle(type, children));

Title.propTypes = {
  type: PropTypes.oneOf(['main', 'subTitle']).isRequired,
  children: PropTypes.node.isRequired,
};

export default Title;
