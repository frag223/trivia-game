import React from 'react';
import PropTypes from 'prop-types';
import styled from 'styled-components';

const Svg = styled.svg`
  fill: ${props => props.fill};
  width: ${props => props.width};
  height: ${props => props.height};
`;

const RightArrow = props => (
  <Svg
    {...props}
    xmlns="http://www.w3.org/2000/svg"
    xmlnsXlink="http://www.w3.org/1999/xlink"
    viewBox="0 0 134.2437232678153 267.95951554329366"
  >
    <defs>
      <path
        d="M134.24 133.98L99.13 108L0 241.98L35.11 267.96L134.24 133.98Z"
        id="c7ApNBUFgn"
      />
      <path
        d="M134.24 133.98L99.13 159.96L0 25.98L35.11 0L134.24 133.98Z"
        id="a1BnxDIAXf"
      />
    </defs>
    <use xlinkHref="#c7ApNBUFgn" />
    <use xlinkHref="#a1BnxDIAXf" />
  </Svg>
);

Svg.propTypes = {
  height: PropTypes.string,
  width: PropTypes.string,
  fill: PropTypes.string,
};
Svg.defaultProps = {
  height: '100%',
  width: '100%',
  fill: 'black',
};

export default RightArrow;
