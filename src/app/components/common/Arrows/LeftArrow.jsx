import React from 'react';
import styled from 'styled-components';

const Svg = styled.svg`
  fill: ${props => props.fill};
  width: ${props => props.width};
  height: ${props => props.height};
`;


const LeftArrow = props => (
  <Svg
    {...props}
    xmlns="http://www.w3.org/2000/svg"
    xmlnsXlink="http://www.w3.org/1999/xlink"
    viewBox="0 0 134.2437232678153 267.95951554329366"
    preserveAspectRatio="xMidYMid meet"
  >
    <defs>
      <path
        d="M0 133.98L35.11 108L134.24 241.98L99.13 267.96L0 133.98Z"
        id="e6TrN19IV5"
      />
      <path
        d="M0 133.98L35.11 159.96L134.24 25.98L99.13 0L0 133.98Z"
        id="a2be7XjPlI"
      />
    </defs>
    <use xlinkHref="#e6TrN19IV5" />
    <use xlinkHref="#a2be7XjPlI" />
  </Svg>
);

Svg.defaultProps = {
  height: '100%',
  width: '100%',
  fill: 'black',
};


export default LeftArrow;
