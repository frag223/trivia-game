import React from 'react';
import PropTypes from 'prop-types';
import styled from 'styled-components';


const Section = styled.section`
  display: flex;
  align-items: center;
  flex-direction: column;
  background-color: white;
  min-width: 350px;
  max-width: 350px;
  white-space: normal;
  margin-top: 3%;
  min-height: 350px;

  @media (max-width: 375px) {
    .card {
      min-width: 350px;
      max-width: 350px;
    }
  }
  @media (min-width: 500px) and (max-width: 700px) {
    .card {
      min-width: 450px;
      max-width: 450px;
    }
  }
`;

const Card = (props) => {
  const { children } = props;

  return (
    <Section className="card">
      {children}
    </Section>
  );
};

Card.propTypes = {
  children: PropTypes.node.isRequired,
};

export default Card;
