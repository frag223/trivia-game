import styled from 'styled-components';
import styledProps from 'styled-props';

export const styleColour = {
  danger: '#DD2C00',
  success: '#7CB342',
  info: '#BBDEFB',
  primary: 'purple',
  default: 'darkgrey',
};

export const borderStyle = {
  round: '50px',
  default: '3px',
};

export const LinkBtn = styled.span`
  color: ${props => (props.primary ? 'white' : styledProps(styleColour, 'color'))}
  display: inline-block;
  font-size: 1em;
  margin: 10px;
  padding: 0.25em 1em;
  border-style: solid;
  border-width: 1px;
  border-color: ${styledProps(styleColour, 'border-colour')};
  border-radius: ${styledProps(borderStyle, 'border-radius')};
  background-color: ${props => (props.primary ? styledProps(styleColour, 'background') : 'white')};
    & > a {
      color: ${props => (props.primary ? 'white' : styledProps(styleColour, 'color'))}
      text-decoration: none;
    }
    & > a:link, a:visited {
      text-decoration: none;
    }
    &:hover {
      opacity: 0.8;
      cursor: pointer;
    }
    &:active {
      color: pink;
    }
`;

LinkBtn.defaultProps = {
  color: 'primary',
  background: 'default',
  'border-colour': 'default',
  'border-radius': 'default',
};

export const Btn = styled.button`
  color: ${props => (props.primary ? 'white' : styledProps(styleColour, 'color'))}
  font-size: 1em;
  margin: 1em;
  padding: 0.25em 1em;
  border-style: solid;
  border-width: 1px;
  border-color: ${styledProps(styleColour, 'border-colour')};
  border-radius: ${styledProps(borderStyle, 'border-radius')};
  background-color: ${props => (props.primary ? styledProps(styleColour, 'background') : 'white')};
    &:hover {
      opacity: 0.8;
      cursor: pointer;
    }
    &:active {
      color: black;
    }
`;

Btn.defaultProps = {
  color: 'primary',
  background: 'default',
  'border-colour': 'default',
  'border-radius': 'default',
};

