import React from 'react';
import PropTypes from 'prop-types';
import styled from 'styled-components';
import styledProps from 'styled-props';
import { Btn, styleColour } from './styles/ButtonStyles';
import LeftArrow from '../Arrows/LeftArrow';
import RightArrow from '../Arrows/RightArrow';


const RoundBtn = styled(Btn)`
  width: 50px;
  height: 50px;
  padding: 0;
  margin: 1em;
  border-radius: 50px;
`;


const StyledLeftArrow = styled(LeftArrow)`
margin-top: 4px;
fill: ${props => (props.primary ? 'white' : styledProps(styleColour, 'fill'))}
&:active {
  fill: black;
}
`;

StyledLeftArrow.defaultProps = {
  fill: 'primary',
};

const StyledRightArrow = styled(RightArrow)`
  margin-top: 4px;
  fill: ${props => (props.primary ? 'white' : styledProps(styleColour, 'fill'))}
  &:active {
    fill: black;
  }
`;

StyledRightArrow.defaultProps = {
  fill: 'primary',
};

const ArrowButton = props => (
  <RoundBtn
    onClick={props.handleClick}
    {...props}
  >
    {props.left ?
      <StyledLeftArrow height="80%" {...props} /> :
      <StyledRightArrow height="80%" {...props} />}
  </RoundBtn>
);

ArrowButton.propTypes = {
  handleClick: PropTypes.func.isRequired,
  left: PropTypes.bool,
  primary: PropTypes.bool,
  info: PropTypes.bool,
  danger: PropTypes.bool,
  success: PropTypes.bool,
};

ArrowButton.defaultProps = {
  left: false,
  primary: false,
  info: false,
  danger: false,
  success: false,
};


export default ArrowButton;
