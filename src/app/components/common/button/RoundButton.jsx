import React from 'react';
import PropTypes from 'prop-types';
import styled from 'styled-components';
import { Btn } from './styles/ButtonStyles';

const RoundBtn = styled(Btn)`
  width: 50px;
  height: 50px;
  padding: 0;
  margin: 0
  border-radius: 50%;
`;

const RoundButton = props => (
  <RoundBtn
    onClick={props.handleClick}
    {...props}
  >
    {props.children}
  </RoundBtn>
);

RoundButton.propTypes = {
  children: PropTypes.node.isRequired,
  handleClick: PropTypes.func.isRequired,
  primary: PropTypes.bool,
  info: PropTypes.bool,
  danger: PropTypes.bool,
  success: PropTypes.bool,
};

RoundButton.defaultProps = {
  primary: false,
  info: false,
  danger: false,
  success: false,
};


export default RoundButton;
