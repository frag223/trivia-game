import React from 'react';
import PropTypes from 'prop-types';
import { Link } from 'react-router-dom';

import { LinkBtn } from './styles/ButtonStyles';

const LinkButton = props => (
  <LinkBtn
    type="button"
    {...props}
  >
    <Link to={props.to}>{props.children}</Link>
  </LinkBtn>
);

LinkButton.propTypes = {
  to: PropTypes.string.isRequired,
  children: PropTypes.node.isRequired,
  primary: PropTypes.bool,
  info: PropTypes.bool,
  danger: PropTypes.bool,
  success: PropTypes.bool,
};

LinkButton.defaultProps = {
  primary: false,
  info: false,
  danger: false,
  success: false,
};


export default LinkButton;
