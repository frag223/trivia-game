import React from 'react';
import PropTypes from 'prop-types';
import { Btn } from './styles/ButtonStyles';


const Button = props => (
  <Btn
    onClick={props.handleClick}
    {...props}
  >
    {props.children}
  </Btn>
);

Button.propTypes = {
  children: PropTypes.node.isRequired,
  handleClick: PropTypes.func.isRequired,
  left: PropTypes.bool,
  primary: PropTypes.bool,
  info: PropTypes.bool,
  danger: PropTypes.bool,
  success: PropTypes.bool,
};

Button.defaultProps = {
  left: false,
  primary: false,
  info: false,
  danger: false,
  success: false,
};


export default Button;
