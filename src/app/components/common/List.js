import React, { Children } from 'react';
import PropTypes from 'prop-types';

const style = {
  listStyleType: 'none',
  padding: '0',
  margin: '0',
  display: 'flex',
  flexDirection: 'column',
};

const List = (props) => {
  const { children } = props;
  return (
    <ul style={style}>
      {Children.map(children, (child, i) => (
        <li key={`list-child-${i}`}>{child}</li>
      ))}
    </ul>
  );
};

List.propTypes = {
  children: PropTypes.node.isRequired,
};

export default List;
