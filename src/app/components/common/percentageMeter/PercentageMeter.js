import React from 'react';
import PropTypes from 'prop-types';

const ReactProgressCircle = ({ percentage, size, colour }) => {
  let appliedRadius;
  let appliedStroke;
  let textSize;
  switch (size) {
    case 'sm':
      appliedRadius = 25;
      appliedStroke = 2.5;
      textSize = 14;
      break;
    case 'md':
      appliedRadius = 50;
      appliedStroke = 5;
      textSize = 26;
      break;
    case 'lg':
      appliedRadius = 75;
      appliedStroke = 7.5;
      textSize = 40;
      break;
    case 'xl':
      appliedRadius = 100;
      appliedStroke = 10;
      textSize = 50;
      break;
    default:
      appliedRadius = 50;
      appliedStroke = 5;
      textSize = 20;
  }

  const normalizedRadius = appliedRadius - (appliedStroke * 2);
  const circumference = normalizedRadius * 2 * Math.PI;
  const strokeDashoffset = circumference - ((percentage / 100) * circumference);

  return (
    <div id="react-progress-circle">
      <svg height={appliedRadius * 2} width={appliedRadius * 2}>
        <circle
          fill="transparent"
          stroke="grey"
          opacity="0.15"
          className="ReactProgressCircle_circleBackground"
          strokeWidth={appliedStroke}
          style={{ strokeDashoffset }}
          r={normalizedRadius}
          cx={appliedRadius}
          cy={appliedRadius}
        />
        <circle
          className="ReactProgressCircle_circle"
          fill="transparent"
          stroke={colour}
          strokeWidth={appliedStroke}
          strokeLinecap="round"
          strokeDasharray={`${circumference} ${circumference}`}
          style={{ strokeDashoffset }}
          r={normalizedRadius}
          cx={appliedRadius}
          cy={appliedRadius}
          transition="stroke-dashoffset 0.8s"
          transform="rotate(-90deg)"
          transform-origin="50% 50%"
        />
        <text
          className="circle-text"
          fill={colour}
          x="50%"
          y="50%"
          dy=".3em"
          fontSize={textSize}
          textAnchor="middle"
        >
          {`${percentage}%`}
        </text>
      </svg>
    </div>
  );
};

ReactProgressCircle.propTypes = {
  percentage: PropTypes.number.isRequired,
  size: PropTypes.oneOf(['sm', 'md', 'lg', 'xl']).isRequired,
  colour: PropTypes.string.isRequired,
};
export default ReactProgressCircle;
