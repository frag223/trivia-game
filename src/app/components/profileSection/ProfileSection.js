import React from 'react';
import PropTypes from 'prop-types';
import Card from '../common/card/Card';

const ProfileSection = ({ children }) => (
  <Card title="Profile">
    {children}
  </Card>
);

ProfileSection.propTypes = {
  children: PropTypes.node.isRequired,
};

export default ProfileSection;
