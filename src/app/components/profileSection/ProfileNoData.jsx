import React, { Fragment } from 'react';

import LinkButton from '../common/button/LinkButton';

const ProfileNoData = () => (
  <Fragment>
    <p>No data to fetch, please start a new game</p>
    <LinkButton primary className="pure-button" to="/new-game">New Game</LinkButton>
  </Fragment>

);

export default ProfileNoData;
