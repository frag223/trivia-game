import React, { Fragment } from 'react';
import PropTypes from 'prop-types';
// components
import PercentageMeter from '../common/percentageMeter/PercentageMeter';

const UserRecordSection = ({ data }) => (
  <Fragment>
    {
      data.map(child => [
        <p key={`category-item-${child.category}`}>{child.category}</p>,
        <PercentageMeter
          key={`percentage-meter-item-${child.category}`}
          percentage={child.percent}
          size={child.size}
          colour={child.colour}
        />,
      ])
    }
  </Fragment>
);

UserRecordSection.propTypes = {
  data: PropTypes.object.isRequired,
};

export default UserRecordSection;
