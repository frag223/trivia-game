import React from 'react';
import PropTypes from 'prop-types';
import styled from 'styled-components';

// components
import Card from '../common/card/Card';
import Title from '../common/Title';

const P = styled.p`
  width: 90%;
`;

const QuestionSection = ({ children, question }) => (
  <Card>
    <Title type="subTitle">Question</Title>
    <P> {question} </P>
    {children}
  </Card>
);

QuestionSection.propTypes = {
  children: PropTypes.node.isRequired,
  question: PropTypes.string.isRequired,
};
export default QuestionSection;
