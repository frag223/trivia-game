import React, { Fragment } from 'react';
import PropTypes from 'prop-types';

// components
import Button from '../common/button/Button';

const QuestionsActions = ({ hidden, id, handleNextQuestion }) => (
  <Fragment>
    <Button
      hidden={!hidden}
      primary
      handleClick={event => handleNextQuestion(id, event)}
    >
      Next Question
    </Button>
  </Fragment>
);

QuestionsActions.propTypes = {
  hidden: PropTypes.bool.isRequired,
  id: PropTypes.number.isRequired,
  handleNextQuestion: PropTypes.func.isRequired,
};
export default QuestionsActions;
