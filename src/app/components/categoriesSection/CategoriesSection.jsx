import React from 'react';
import PropTypes from 'prop-types';
import styled from 'styled-components';
import List from '../common/List';
import ArrowButton from '../common/button/ArrowButton';
import Card from '../common/card/Card';
import Title from '../common/Title';

const Div = styled.div`
  display: flex;
  width: 100%;
  height: 100%;
  align-items: center;
  justify-content: space-between;
  text-align: center;
`;

const CategoriesSection = (props) => {
  const { children, handlePreviousClick, handleNextClick } = props;

  return (
    <Card title="Categories Section">
      <Title type="subTitle">Categories</Title>
      <Div className="categories-section" >
        <ArrowButton
          left
          handleClick={handlePreviousClick}
        />
        <List>
          {
            children
          }
        </List>
        <ArrowButton
          handleClick={handleNextClick}
        />
      </Div>
    </Card>
  );
};

CategoriesSection.propTypes = {
  children: PropTypes.node.isRequired,
  handlePreviousClick: PropTypes.func.isRequired,
  handleNextClick: PropTypes.func.isRequired,
};
CategoriesSection.defaultProps = {
  categories: [],
};

export default CategoriesSection;
