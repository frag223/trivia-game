import React from 'react';
import PropTypes from 'prop-types';
// components
import Card from './common/card/Card';
import List from './common/List';
import LinkButton from './common/button/LinkButton';


const Instructions = ({ disabled }) => (
  <Card title="Instructions" className="instructions">
    <List>
      <p>1. Click New Game</p>
      <p>2. ?????</p>
      <p>3. Profit</p>
    </List>
    <LinkButton primary to="/new-game" className={`pure-button ${disabled}`}>New Game</LinkButton>
  </Card>
);

Instructions.propTypes = {
  disabled: PropTypes.string.isRequired,
};

export default Instructions;
