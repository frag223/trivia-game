import React from 'react';
import PropTypes from 'prop-types';

// components

// css
import './answersSection.css';

const AnswersSection = ({ children }) => (
  <section className="answer-section">
    {children}
  </section>
);

AnswersSection.propTypes = {
  children: PropTypes.node.isRequired,
};

export default AnswersSection;
