import React from 'react';
// components
import Card from './common/card/Card';
import LinkButton from './common/button/LinkButton';

const CompleteSection = () => (
  <Card title="Complete">
    <p>You have completed these set of questions, click on the categories link to select a new category</p>
    <LinkButton primary to="/categories">Categories</LinkButton>
  </Card>
);

export default CompleteSection;
