import React from 'react';
import { Link } from 'react-router-dom';

// components
import Title from '../common/Title';
// css
import './menuSection.css';

const MenuSection = () => (
  <section className="main-title shadow">
    <Title type="main">Simple Trivia Game</Title>
    <nav className="menu-section">
      <Link to="/profile" ><i className="fas fa-user fa-lg" /></Link>
      <Link to="/categories" ><i className="fas fa-book-open fa-lg" /></Link>
      <Link to="/settings" ><i className="fas fa-cog fa-lg" /></Link>
    </nav>
  </section>
);

export default MenuSection;
