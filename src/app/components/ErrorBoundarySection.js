import React from 'react';
// components
import Card from './common/Card/Card';

const ErrorBoundarySection = () => (
  <Card title="Error">
    <p>An Error has occured, sorry</p>
  </Card>
);

export default ErrorBoundarySection;
