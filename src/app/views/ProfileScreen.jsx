import React, { lazy, Suspense } from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';

// components
import ProfileSection from '../components/profileSection/ProfileSection';
import ProfileNoData from '../components/profileSection/ProfileNoData';
import Loader from '../components/common/loader/LoadingCard';

// selectors
import {
  generateChartData,
} from '../reducers/history';

const UserRecordSection = lazy(() => import('../components/profileSection/UserRecord'));


const ProfileContainer = ({ chartData }) => (
  <Suspense fallback={<Loader />}>
    <ProfileSection>
      {
        chartData.length === 0 ?
          <ProfileNoData /> :
          <UserRecordSection data={chartData} />
      }
    </ProfileSection>
  </Suspense>
);


const mapStateToProps = (state) => {
  const history = state.history;
  const chartData = generateChartData(history, 'sm');
  return {
    history: state.history,
    chartData,
  };
};

ProfileContainer.propTypes = {
  chartData: PropTypes.array,
};

ProfileContainer.defaultProps = {
  chartData: [],
};

export default connect(
  mapStateToProps,
)(ProfileContainer);
