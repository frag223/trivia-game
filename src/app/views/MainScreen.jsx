import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';

// actions
import fetchSessionToken from '../actions/sessionToken';
// Components
import Instructions from '../components/Instructions';
import NotificationSection from '../components/notificationSection/NotificationSection';

class MainScreen extends Component {
  componentDidMount() {
    return this.props.fetchSessionToken();
  }

  isTokenLoading() {
    let loading = 'pure-button-disabled';
    if (!this.props.token.loading) {
      loading = '';
    }
    return loading;
  }

  render() {
    return [
      <NotificationSection key="message-section">
        { this.isTokenLoading() ? 'Generating Token...' : 'Token Generated' }
      </NotificationSection>,
      <Instructions key="instructions" disabled={this.isTokenLoading()} />,
    ];
  }
}

MainScreen.propTypes = {
  fetchSessionToken: PropTypes.func.isRequired,
  token: PropTypes.shape({
    loading: PropTypes.bool,
    token: PropTypes.string,
  }).isRequired,
};

const mapStateToProps = state => ({
  token: state.token,
});

const mapDispatchToProps = {
  fetchSessionToken,
};

export default connect(
  mapStateToProps,
  mapDispatchToProps,
)(MainScreen);
