import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';

import addCategories from '../actions/categories';

// Containers
import CategoriesContainer from '../containers/CategoriesContainer';
import NotificationSection from '../components/notificationSection/NotificationSection';

class CategoriesScreen extends Component {
  static propTypes = {
    addCategories: PropTypes.func.isRequired,
    categories: PropTypes.shape({
      loading: PropTypes.bool,
      categories: PropTypes.arrayOf(PropTypes.object),
    }).isRequired,
  }
  componentDidMount() {
    if (this.props.categories.loading === undefined) {
      return this.props.addCategories();
    }
    return '';
  }
  infoMessage() {
    let message = 'Fetching Categories';
    if (this.props.categories.loading === false) {
      const totalCategories = this.props.categories.categories.length;
      message = `Total Categories Returned: ${totalCategories}`;
    }
    return message;
  }
  render() {
    return [
      <NotificationSection key="notification-category">{this.infoMessage()}</NotificationSection>,
      <CategoriesContainer key="categories-container" />,
    ];
  }
}


const mapStateToProps = state => ({
  categories: state.categories,
});

const mapDispatchToProps = {
  addCategories,
};


export default connect(
  mapStateToProps,
  mapDispatchToProps,
)(CategoriesScreen);
