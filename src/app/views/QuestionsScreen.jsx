import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';

// actions
import { fetchQuestions } from '../actions/questions';

// components
import NotificationSection from '../components/notificationSection/NotificationSection';

// containers
import QuestionsContainer from '../containers/QuestionsContainer';

class QuestionsScreen extends Component {
  static propTypes = {
    token: PropTypes.shape({
      loading: PropTypes.bool.isRequired,
      token: PropTypes.string.isRequired,
    }).isRequired,
    match: PropTypes.shape({
      params: PropTypes.object.isRequired,
    }).isRequired,
    fetchQuestions: PropTypes.func.isRequired,
    questions: PropTypes.shape({
      loading: PropTypes.bool,
      questions: PropTypes.array,
    }).isRequired,
  }

  static defaultProps = {
    questions: {
      loading: true,
      questions: [],
    },
  }

  componentDidMount() {
    const parameterId = this.props.match.params.id;
    const params = {
      token: this.props.token.token,
      category: parseInt(parameterId, 10),
    };
    return this.props.fetchQuestions(params);
  }

  numberOfQuestionsRemaining() {
    if (this.props.questions.loading === false) {
      const numberQuestions = this.props.questions.questions.length;
      return `Questions remaining: ${numberQuestions}`;
    }
    return 'Loading questions...';
  }

  render() {
    return [
      <NotificationSection key="question-notification">
        {this.numberOfQuestionsRemaining()}
      </NotificationSection>,
      <QuestionsContainer key="question-container" />,
    ];
  }
}


const mapStateToProps = state => ({
  token: state.token,
  questions: state.questions,
});

const mapDispatchToProps = {
  fetchQuestions,
};


export default connect(
  mapStateToProps,
  mapDispatchToProps,
)(QuestionsScreen);
