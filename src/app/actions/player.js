import {
  UPDATE_PLAYER_SUCCESS,
  UPDATE_PLAYER_ERROR,
  UPDATE_PLAYER_LOADING,
} from '../config/constants';

const updatePlayerSuccess = player => ({
  type: UPDATE_PLAYER_SUCCESS,
  player,
});
const updatePlayerError = (bool, message) => ({
  type: UPDATE_PLAYER_ERROR,
  error: bool,
  message,
});

const updatePlayerLoading = bool => ({
  type: UPDATE_PLAYER_LOADING,
  loading: bool,
});

export default function updatePlayer(player) {
  return (dispatch) => {
    dispatch(updatePlayerLoading(true));
    try {
      dispatch(updatePlayerLoading(false));
      dispatch(updatePlayerSuccess(player));
    } catch (error) {
      dispatch(updatePlayerLoading(false));
      dispatch(updatePlayerError(true));
    }
  };
}
