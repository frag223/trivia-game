/* @flow */
import type {
  HistoryErrorType,
  HistoryQuestionType,
  HistoryLoadingType,
  Dispatch,
  GetState,
} from 'history';

import {
  ADD_HISTORY_LOADING,
  ADD_HISTORY_ERROR,
  ADD_HISTORY_SUCCESS,
} from '../config/constants';

import { historyMerge } from '../reducers/history';

const historyLoading = (bool: boolean): HistoryLoadingType => ({
  type: ADD_HISTORY_LOADING,
  loading: bool,
});

const historyError = (bool: boolean, message: Error): HistoryErrorType => ({
  type: ADD_HISTORY_ERROR,
  error: bool,
  message,
});

const historySuccess = (history: HistoryQuestionType[]) => ({
  type: ADD_HISTORY_SUCCESS,
  history,
});

export const addToHistory = (question: HistoryQuestionType) => (dispatch: Dispatch, getState: GetState): void => {
  dispatch(historyLoading(true));
  try {
    const state = getState();
    const history = state.history.history;
    const newHistory = historyMerge(history, question);
    dispatch(historySuccess(newHistory));
    dispatch(historyLoading(false));
  } catch (error) {
    throw Error(error);
  }
};

export default function updateHistoryList(history: HistoryQuestionType[]) {
  return (dispatch: Dispatch): void => {
    dispatch(historyLoading(true));
    try {
      dispatch(historySuccess(history));
      dispatch(historyLoading(false));
    } catch (error) {
      dispatch(historyError(true, error));
      dispatch(historyLoading(false));
    }
  };
}
