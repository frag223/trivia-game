/* @flow */

import type {
  QuestionErrorType,
  QuestionLoadingType,
  QuestionSuccessType,
  ClearQuestionsSuccess,
  GetQuestionParametersType,
  Action,
  State,
  GetState,
  PromiseAction,
  ThunkAction,
  Dispatch,
  QuestionType,
} from 'questions';

import {
  ADD_QUESTIONS_ERROR,
  ADD_QUESTIONS_LOADING,
  ADD_QUESTIONS_SUCCESS,
  CLEAR_QUESTIONS_SUCCESS,
  UPDATE_QUESTION_LOADING,
  UPDATE_QUESTION_SUCCESS,
  UPDATE_QUESTION_ERROR,
} from '../config/constants';

import { transformQuestion } from '../lib/transform';

import { updateQuestionProperties, removeQuestionById, unshiftQuestionList } from '../reducers/questions';

const addQuestionsError = (bool: boolean, message: Error): QuestionErrorType => ({
  type: ADD_QUESTIONS_ERROR,
  error: bool,
  message,
});

export const addQuestionsLoading = (bool: boolean): QuestionLoadingType => ({
  type: ADD_QUESTIONS_LOADING,
  loading: bool,
});

const addQuestionsSuccess = (questions: QuestionType[]): QuestionSuccessType => ({
  type: ADD_QUESTIONS_SUCCESS,
  questions,
});

const clearQuestionsSuccess = (): ClearQuestionsSuccess => ({
  type: CLEAR_QUESTIONS_SUCCESS,
});

const updateQuestionsLoading = (bool: boolean): QuestionLoadingType => ({
  type: UPDATE_QUESTION_LOADING,
  loading: bool,
});

const updateQuestionsSuccess = (questions: QuestionType[]): QuestionSuccessType => ({
  type: UPDATE_QUESTION_SUCCESS,
  questions,
});

const updateQuestionsError = (bool: boolean, message: Error): QuestionErrorType => ({
  type: UPDATE_QUESTION_ERROR,
  error: bool,
  message,
});

export const updateQuestionAction = (question: QuestionType, answer: string) => (dispatch: Dispatch, getState: GetState) => {
  dispatch(addQuestionsLoading(true));
  try {
    const state: State = getState();
    const questionsArray = state.questions.questions;
    const updatedQuestion = updateQuestionProperties(question, answer);
    const filteredList = removeQuestionById(questionsArray, question.id);
    const newQuestionList = unshiftQuestionList(updatedQuestion, filteredList);
    dispatch(updateQuestionsSuccess(newQuestionList));
    dispatch(addQuestionsLoading(false));
  } catch (error) {
    throw Error(error);
  }
};

export const fetchQuestions = (params: GetQuestionParametersType) => (dispatch: Dispatch, getState: GetState, { trivia }: any): void => {
  dispatch(addQuestionsLoading(true));
  return trivia.questions(params)
    .then(encodedResponse => encodedResponse.results.map(transformQuestion))
    .then((response) => {
      dispatch(addQuestionsSuccess(response));
      dispatch(addQuestionsLoading(false));
    })
    .catch((error) => {
      dispatch(addQuestionsError(true, error));
      dispatch(addQuestionsLoading(false));
    });
};


export const updateQuestionsList = (questions: QuestionType[]) => (dispatch: Dispatch): void => {
  dispatch(updateQuestionsLoading(true));
  try {
    dispatch(updateQuestionsSuccess(questions));
    dispatch(addQuestionsLoading(false));
  } catch (error) {
    dispatch(updateQuestionsError(true, error));
    dispatch(addQuestionsLoading(false));
  }
};

export const clearQuestions = () => (dispatch: Dispatch): void => {
  dispatch(clearQuestionsSuccess());
};
