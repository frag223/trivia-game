/* @flow */

import {
  UPDATE_SCORE_LOADING,
  UPDATE_SCORE_SUCCESS,
  UPDATE_SCORE_ERROR,
} from '../config/constants';

import type {
  ScoreErrorType,
  ScoreLoadingType,
  ScoreSuccessType,
  Action,
  State,
  GetState,
  PromiseAction,
  ThunkAction,
  Dispatch,
} from 'score';

const scoreLoading = (bool: boolean): ScoreLoadingType => ({
  type: UPDATE_SCORE_LOADING,
  loading: bool,
});

const scoreError = (bool: boolean, message: Error): ScoreErrorType => ({
  type: UPDATE_SCORE_ERROR,
  error: bool,
  message,
});

const scoreSuccess = (score: number): ScoreSuccessType => ({
  type: UPDATE_SCORE_SUCCESS,
  score,
});

const addScore = (currentScore: number): number => currentScore += 1;

export default function updateScore() {
  return (dispatch: Dispatch, getState: GetState) => {
    dispatch(scoreLoading(true));
    const getCurrentState = getState();
    const currentScore = getCurrentState.score.score || 0;
    const updatedScore = addScore(currentScore);
    try {
      dispatch(scoreSuccess(updatedScore));
      dispatch(scoreLoading(false));
    } catch (error) {
      dispatch(scoreError(true, error));
      dispatch(scoreLoading(false));
    }
  };
}
