/* @flow */

import {
  REQUEST_TOKEN_ERROR,
  REQUEST_TOKEN_LOADING,
  REQUEST_TOKEN_SUCCESS,
} from '../config/constants';

import type {
  TokenErrorType,
  TokenLoadingType,
  TokenSuccessType,
  Action,
  State,
  GetState,
  PromiseAction,
  ThunkAction,
  Dispatch,
} from 'token';

const tokenLoading = (bool: boolean): TokenLoadingType => ({
  type: REQUEST_TOKEN_LOADING,
  loading: bool,
});

const tokenError = (bool: boolean, message: Error): TokenErrorType => ({
  type: REQUEST_TOKEN_ERROR,
  error: bool,
  message,
});

const tokenSuccess = (token: string): TokenSuccessType => ({
  type: REQUEST_TOKEN_SUCCESS,
  token,
});

export default function fetchSessionToken() {
  return (dispatch: Dispatch, getState: GetState, { auth }: any): void => {
    dispatch(tokenLoading(true));
    return auth.generateToken()
      .then((token) => {
        dispatch(tokenLoading(false));
        dispatch(tokenSuccess(token));
      })
      .catch((error) => {
        dispatch(tokenLoading(false));
        dispatch(tokenError(true, error));
      });
  };
}

