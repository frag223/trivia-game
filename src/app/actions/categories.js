import {
  ADD_CATEGORIES_ERROR,
  ADD_CATEGORIES_LOADING,
  ADD_CATEGORIES_SUCCESS,
} from '../config/constants';
import { transformCategory } from '../lib/transform';


const categoriesError = (bool, message) => ({
  type: ADD_CATEGORIES_ERROR,
  error: bool,
  message,
});


export const categoriesLoading = bool => ({
  type: ADD_CATEGORIES_LOADING,
  loading: bool,
});

export const categoriesSuccess = categories => ({
  type: ADD_CATEGORIES_SUCCESS,
  categories,
});

export default function addCategories() {
  return (dispatch, getState, { trivia }) => {
    dispatch(categoriesLoading(true));
    return trivia.categories()
      .then(categories => categories.map(item => transformCategory(item)))
      .then((transformedCategories) => {
        dispatch(categoriesSuccess(transformedCategories));
        dispatch(categoriesLoading(false));
      })
      .catch((error) => {
        dispatch(categoriesError(true, error));
        dispatch(categoriesLoading(false));
      });
  };
}
