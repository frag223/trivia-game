/* @flow */

import type { triviaClientType } from 'triviaClient';
import { TRIVIA_URL, MAX_QUESTIONS_COUNT } from '../../config/constants';
import validateResponse from '../../lib/validateResponse';


export default class Trivia {
  url: string;
  client: triviaClientType;
  encoding: string;
  constructor(client: triviaClientType) {
    this.url = TRIVIA_URL;
    this.client = client;
    this.encoding = 'base64';
  }

  categories(): Promise<Object> {
    const url = `${this.url}/api_category.php`;
    return this.client(url)
      // eslint-disable-next-line camelcase
      .then(({ trivia_categories }) => trivia_categories)
      .catch((error) => {
        throw error;
      });
  }

  questions({ category, token }: { category: string, token: string}): Promise<Object> {
    const amount = MAX_QUESTIONS_COUNT;
    const url = `${this.url}/api.php?` +
      `amount=${amount}` +
      `&category=${category}` +
      `&token=${token}&encode=${this.encoding}`;
    return this.client(url)
      .then(data => validateResponse(data))
      .catch((error) => {
        throw error;
      });
  }
}
