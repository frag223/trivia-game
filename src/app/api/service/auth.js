/* @flow */

import type { triviaClientType } from 'triviaClient';

import validateResponse from '../../lib/validateResponse';

export default class Auth {
  client: triviaClientType;
  baseUrl: string;

  constructor(client: triviaClientType, baseUrl: string) {
    this.client = client;
    this.baseUrl = baseUrl;
  }

  generateToken() {
    const tokenUrl = `${this.baseUrl}/api_token.php?command=request`;
    return this.client(tokenUrl)
      .then((data) => {
        const { token } = validateResponse(data);
        return token;
      })
      .catch((error) => {
        throw error;
      });
  }

  resetToken(sessionToken: string) {
    const tokenUrl = `${this.baseUrl}/api_token.php?command=reset&token=${sessionToken}`;
    return this.client(tokenUrl)
      .then((data) => {
        const { token } = validateResponse(data);
        return token;
      })
      .catch((error) => {
        throw error;
      });
  }
}
