/* @flow */

const triviaClient = (url: string) => new Promise((resolve, reject) => fetch(url)
  .then(response => response.json())
  .then((parseddata) => {
    resolve(parseddata);
  })
  .catch((error) => {
    reject(error);
  }));
export default triviaClient;
