import { createStore, applyMiddleware } from 'redux';
import thunk from 'redux-thunk';
import rootReducer from '../reducers/rootReducer';

import { TRIVIA_URL } from '../config/constants';
import TriviaClient from '../api/client/triviaClient';
import Auth from '../api/service/auth';
import Trivia from '../api/service/trivia';

const auth = new Auth(TriviaClient, TRIVIA_URL);
const trivia = new Trivia(TriviaClient);

export default function configureStore() {
  return createStore(
    rootReducer,
    applyMiddleware(thunk.withExtraArgument({ auth, trivia })),
  );
}
