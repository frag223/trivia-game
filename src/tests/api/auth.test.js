import Auth from '../../app/api/service/auth';
import MockClient from '../mocks/TriviaClient.mock';
import { TRIVIA_URL } from '../../app/config/constants';

import validTokenResponse from '../data/valid/valid-tokenRequestResponse.json';
import invalidTokenResponse from '../data/invalid/invalid-tokenRequestResponse.json';


describe('Trivia Auth', () => {
  describe('generateToken()', () => {
    it('valid response should return a token', () => {
      const client = MockClient(validTokenResponse, false).triviaClient;
      const trivia = new Auth(client, TRIVIA_URL);
      return trivia.generateToken()
        .then((data) => {
          expect(data).toEqual(validTokenResponse.token);
        });
    });

    it('invalid response should return error', () => {
      const client = MockClient(invalidTokenResponse, true).triviaClient;
      const trivia = new Auth(client, TRIVIA_URL);
      return trivia.generateToken()
        .catch((error) => {
          expect(error).toHaveProperty('response_code');
          expect(error).toHaveProperty('response_message');
        });
    });
  });

  describe('resetToken()', () => {
    it('should return token', () => {
      const client = MockClient(validTokenResponse, false).triviaClient;
      const trivia = new Auth(client, TRIVIA_URL);
      return trivia.resetToken('abcdef')
        .then((token) => {
          expect(token).toEqual(expect.not.stringMatching('abcdef'));
        });
    });
  });
});
