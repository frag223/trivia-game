import Trivia from '../../app/api/service/trivia';
import MockClient from '../mocks/TriviaClient.mock';
import { decode } from '../../app/lib/utils/decode';

import validCategoriesResponse from '../data/valid/valid-categoiesData.json';
import validTriviaResponsePayload from '../data/valid/valid-triviaResponsePayload.json';


describe('Trivia api', () => {
  describe('categories()', () => {
    it('should return a list of categories', () => {
      const client = MockClient(validCategoriesResponse, false).triviaClient;
      const trivia = new Trivia(client);
      return trivia.categories()
        .then((data) => {
          const test = data[0];
          expect(test).toHaveProperty('id');
          expect(test).toHaveProperty('name');
        });
    });
  });

  describe('questions()', () => {
    it('should return a list of question', () => {
      const client = MockClient(validTriviaResponsePayload, false).triviaClient;
      const trivia = new Trivia(client);
      const params = {
        amount: 5,
        category: 1,
        token: 'abcdef',
      };
      return trivia.questions(params)
        .then((data) => {
          const test = data;
          expect(test).toHaveProperty('results');
          expect(decode(test.results[0].category)).toEqual('General Knowledge');
        });
    });
  });
});
