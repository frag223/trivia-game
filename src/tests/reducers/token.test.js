import token from '../../app/reducers/token';
import {
  REQUEST_TOKEN_ERROR,
  REQUEST_TOKEN_SUCCESS,
  REQUEST_TOKEN_LOADING,
} from '../../app/config/constants';


describe('Token reducers', () => {
  it('Loading action returns bool', () => {
    const loadingAction = {
      type: REQUEST_TOKEN_LOADING,
      loading: true,
    };
    expect(token({}, loadingAction)).toHaveProperty('loading');
    expect(token({}, loadingAction).loading).toEqual(true);
  });

  it('Error action returns error bool and message ', () => {
    const errorAction = {
      type: REQUEST_TOKEN_ERROR,
      error: true,
      message: 'expected error',
    };

    expect(token({}, errorAction)).toHaveProperty('error');
    expect(token({}, errorAction)).toHaveProperty('message');
    expect(token({}, errorAction).error).toEqual(true);
  });

  it('Success Action returns token', () => {
    const successAction = {
      type: REQUEST_TOKEN_SUCCESS,
      token: 'your-token',
    };

    expect(token({}, successAction)).toHaveProperty('token');
    expect(token({}, successAction).token).toEqual('your-token');
  });
});
