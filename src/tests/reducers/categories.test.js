import categories, { paginateCategories } from '../../app/reducers/categories';
import {
  ADD_CATEGORIES_ERROR,
  ADD_CATEGORIES_SUCCESS,
  ADD_CATEGORIES_LOADING,
} from '../../app/config/constants';

describe('Categories Reducer', () => {
  it('Loading action returns loading true', () => {
    const loadingAction = {
      type: ADD_CATEGORIES_LOADING,
      loading: true,
    };
    expect(categories({}, loadingAction)).toHaveProperty('loading');
    expect(categories({}, loadingAction).loading).toEqual(true);
  });

  it('Error action returns error bool and error message', () => {
    const errorMessage = {
      type: ADD_CATEGORIES_ERROR,
      error: true,
      message: 'expected message',
    };
    expect(categories({}, errorMessage)).toHaveProperty('error');
    expect(categories({}, errorMessage)).toHaveProperty('message');
    expect(categories({}, errorMessage).error).toEqual(true);
  });

  it('Success action returns categories', () => {
    const successAction = {
      type: ADD_CATEGORIES_SUCCESS,
      categories: [{ id: 1, name: 'sports' }],
    };
    expect(categories({}, successAction)).toHaveProperty('categories');
    expect(categories({}, successAction).categories[0]).toHaveProperty('id');
    expect(categories({}, successAction).categories[0]).toHaveProperty('name');
  });

  describe('Selectors', () => {
    it('paginateCategories() should return a nested array of 2 arrays, each has 5 items', () => {
      const state = {
        loading: false,
        categories: [1, 2, 3, 4, 5, 6, 7, 8, 9, 10],
      };
      const testArray = paginateCategories(state);
      expect(testArray.length).toEqual(2);
      expect(testArray[0].length).toEqual(5);
      expect(testArray[1].length).toEqual(5);
    });
  });
});
