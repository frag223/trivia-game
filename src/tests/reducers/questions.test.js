import type { QuestionType } from 'questions';
import type { HistoryQuestionType } from 'history';
import questions,
{
  filterQuestionsById,
  checkIfAnswerIsCorrect,
  getPossibleAnswers,
  findIndexById,
  removeQuestionById,
  addQuestionProperties,
  updateQuestionProperties,
  unshiftQuestionList,
} from '../../app/reducers/questions';

import {
  ADD_QUESTIONS_ERROR,
  ADD_QUESTIONS_LOADING,
  ADD_QUESTIONS_SUCCESS,
  UPDATE_QUESTION_SUCCESS,
  CLEAR_QUESTIONS_SUCCESS,
} from '../../app/config/constants';


describe('Questions Reducers', () => {
  it('Loading action returns loading true', () => {
    const loadingAction = {
      type: ADD_QUESTIONS_LOADING,
      loading: true,
    };
    expect(questions({}, loadingAction)).toHaveProperty('loading');
    expect(questions({}, loadingAction).loading).toEqual(true);
  });

  it('Error action returns error: bool and message', () => {
    const errorAction = {
      type: ADD_QUESTIONS_ERROR,
      error: true,
      message: 'expected error',
    };
    expect(questions({}, errorAction)).toHaveProperty('error');
    expect(questions({}, errorAction)).toHaveProperty('message');
    expect(questions({}, errorAction).error).toEqual(true);
  });

  it('Success action returns questions ', () => {
    const successAction = {
      type: ADD_QUESTIONS_SUCCESS,
      questions: [{ id: 1, question: 'hello' }],
    };
    expect(questions({}, successAction)).toHaveProperty('questions');
    expect(questions({}, successAction).questions[0]).toHaveProperty('id');
    expect(questions({}, successAction).questions[0]).toHaveProperty('question');
  });
});

describe('Questions Selectors', () => {
  const initialState = {
    questions: [
      { id: 1, question: 'hello', correct_answer: 'dono', incorrect_answers: ['wrong answer'] },
      { id: 2, question: 'world', correct_answer: 'why', incorrect_answers: ['wrong hello'] },
      { id: 3, question: 'bar', correct_answer: 'this', incorrect_answers: ['wrong blah'] },
    ],
  };

  it('filterQuestionsById() valid params should return an array of 1', () => {
    const test = filterQuestionsById(initialState.questions, 1);
    expect(test).toHaveProperty('id');
  });

  it('findIndexById() valid params should return index of question', () => {
    const stuff = initialState.questions;
    const test = findIndexById(stuff, 2);
    expect(test).toEqual(1);
  });

  it('removeQuestionById() valid params should remove question and return new array', () => {
    const stuff = initialState.questions;
    const test = removeQuestionById(stuff, 3);
    expect(test.length).toEqual(2);
    test.forEach((item) => {
      expect(item).not.toHaveProperty('id', 3);
    });
  });

  it('addQuestionProperties() should return new properties: answered, correct ', () => {
    const question = initialState.questions[0];
    const updatedQuestion = addQuestionProperties(question, false, 'slap');
    expect(updatedQuestion).toHaveProperty('answered', true);
    expect(updatedQuestion).toHaveProperty('submitedAnswer', 'slap');
    expect(updatedQuestion).toHaveProperty('correct', false);
  });

  it('updateQuestionProperties() correct answer should return: answered true, correct true', () => {
    const question = initialState.questions[0];
    const updatedQuestion = updateQuestionProperties(question, 'dono');
    expect(updatedQuestion).toHaveProperty('answered', true);
    expect(updatedQuestion).toHaveProperty('submitedAnswer', 'dono');
    expect(updatedQuestion).toHaveProperty('correct', true);
  });

  it('updateQuestion() correct answer should return: answered true, correct false ', () => {
    const question = initialState.questions[0];
    const updatedQuestion = updateQuestionProperties(question, 'fake answer');
    expect(updatedQuestion).toHaveProperty('answered', true);
    expect(updatedQuestion).toHaveProperty('correct', false);
  });

  it('unshiftQuestionList() should return a new array with a new entry at the front ', () => {
    const questions = initialState.questions;
    const updatedQuestion = { id: 7, question: 'new entry', correct_answer: 'silly', incorrect_answers: ['super'] };
    const updatedQuestionList = unshiftQuestionList(updatedQuestion, questions);
    expect(updatedQuestionList[0]).toHaveProperty('id', 7);
  });
});
