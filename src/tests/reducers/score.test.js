
import score from '../../app/reducers/score';
import {
  UPDATE_SCORE_ERROR,
  UPDATE_SCORE_LOADING,
  UPDATE_SCORE_SUCCESS,
} from '../../app/config/constants';


describe('Score Reducer', () => {
  it('UPDATE_SCORE_ERROR action returns error message', () => {
    const failAction = {
      type: UPDATE_SCORE_ERROR,
      error: true,
      message: 'expectedMessage',
    };

    expect(score({}, failAction)).toHaveProperty('error');
    expect(score({}, failAction)).toHaveProperty('message');
  });

  it('UPDATE_SCORE_LOADING Action returns loading true', () => {
    const loadingAction = {
      type: UPDATE_SCORE_LOADING,
      loading: true,
    };
    expect(score({}, loadingAction)).toHaveProperty('loading');
    expect(score({}, loadingAction).loading).toEqual(true);
  });

  it('UPDATE_SCORE_SUCCESS Action returns score', () => {
    const successAction = {
      type: UPDATE_SCORE_SUCCESS,
      score: 1,
    };
    expect(score({}, successAction)).toHaveProperty('score');
    expect(score({}, successAction).score).toEqual(1);
  });
});
