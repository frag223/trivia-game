import {
  filterByCategory,
  getAllCategories,
  transformData,
} from '../../app/reducers/history';


describe('History Reducers', () => {

});

describe('History Selectors', () => {
  const initialState = {
    history: [
      {
        id: 1,
        category: 'Science: Computers',
        answered: true,
        correct: true,
        question: 'hello',
        correct_answer: 'dono',
        incorrect_answers: ['wrong answer'],
      },
      {
        id: 2,
        category: 'Science: Gadgets',
        answered: true,
        correct: false,
        question: 'world',
        correct_answer: 'why',
        incorrect_answers: ['wrong hello'],
      },
      {
        id: 3,
        category: 'Science: Computers',
        answered: true,
        correct: true,
        question: 'bar',
        correct_answer: 'this',
        incorrect_answers: ['wrong blah'],
      },
    ],
  };

  it('filterByCategory() filter by Science: Computers should return 2', () => {
    const test = filterByCategory(initialState.history, 'Science: Computers');
    expect(test.length).toEqual(2);
  });

  it('filterByCategory() filter by Science: Gadgets should return 1', () => {
    const test = filterByCategory(initialState.history, 'Science: Gadgets');
    expect(test.length).toEqual(1);
  });

  it('getAllCategories() should return an array of categories', () => {
    const test = getAllCategories(initialState.history);
    expect(test.length).toEqual(2);
    expect(test[0]).toEqual('Science: Computers');
  });

  it('transformData() should return an array of Chart Data Type', () => {
    const test = transformData(initialState.history);
    expect(test[0]).toHaveProperty('id');
    expect(test[0]).toHaveProperty('category');
    expect(test[0]).toHaveProperty('questions');
    expect(test[0]).toHaveProperty('correct');
    expect(test[0].correct).toEqual(2);
    expect(test[1].correct).toEqual(0);
  });
});
