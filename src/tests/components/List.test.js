import React from 'react';
import { shallow } from 'enzyme';
import List from '../../app/components/common/List';

describe('<List />', () => {
  const wrapper = shallow(
    <List >
      <p>test</p>
    </List>,
  );
  it('Should return a ul html element', () => {
    expect(wrapper.find('ul').exists()).toBeTruthy();
  });

  it('Should return children', () => {
    expect(wrapper.find('p').props().children).toEqual('test');
  });

  it('Should return li element', () => {
    expect(wrapper.find('li').length).toEqual(1);
  });
});
