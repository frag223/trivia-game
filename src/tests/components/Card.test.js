import React from 'react';
import { shallow } from 'enzyme';
import Card from '../../app/components/common/Card';

describe('<Card />', () => {
  const wrapper = shallow(
    <Card title="Test Title">
      <p>test</p>
    </Card>,
  );
  it('Should return a Title component', () => {
    expect(wrapper.find('Title').exists()).toBeTruthy();
  });

  it('Children should return', () => {
    expect(wrapper.find('p').props().children).toEqual('test');
  });
});
