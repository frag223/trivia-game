import React from 'react';
import { shallow, mount, render } from 'enzyme';
import Title from '../../app/components/common/Title';

describe('<Title />', () => {
  it('prop type main should return h1 title', () => {
    const wrapper = shallow(
      <Title type="main">Hello</Title>,
    );
    expect(wrapper.find('h1').exists()).toBeTruthy();
  });
  it('prop type subtitle should return h3 title', () => {
    const wrapper = shallow(
      <Title type="subTitle">Hello</Title>,
    );
    expect(wrapper.find('h3').exists()).toBeTruthy();
  });
  it('should render children', () => {
    const wrapper = shallow(
      <Title type="main">Hello</Title>,
    );
    expect(wrapper.props().children).toEqual('Hello');
  });
});
