import React from 'react';
import { shallow } from 'enzyme';
import Button from '../../app/components/common/button/Button';

describe('<Button />', () => {
  const wrapper = shallow(
    <Button type="navigation" handleClick={() => 'hello world'}>Hello</Button>,
  );
  it('Should return a button html element', () => {
    expect(wrapper.find('button').exists()).toBeTruthy();
  });

  it('Should return children', () => {
    expect(wrapper.find('button').props().children).toEqual('Hello');
  });
});
