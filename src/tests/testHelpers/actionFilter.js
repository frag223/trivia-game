/* @flow */

/**
 * filters actions by expected type
 * @param {array} actionArray
 * @param {string} expectedAction
 */
export default function (actionArray: any[], expectedAction: string): any[] {
  return actionArray.filter((action: any) => action.type === expectedAction);
}
