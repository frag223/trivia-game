import 'jest';
import { decodeObject, decode } from '../../app/lib/utils/decode';

import validTriviaResponsePayload from '../data/valid/valid-triviaResponsePayload.json';


describe('Transform Api', () => {
  describe('decode()', () => {
    it('should return message decoded', () => {
      const message = 'dGhpcy1pcy1lbmNvZGVk';
      const test = decode(message);
      expect(test).toBe('this-is-encoded');
    });
  });
  describe('decodeObject()', () => {
    it('should return an object with properties decoded', () => {
      const question = validTriviaResponsePayload.results[0];
      const test = decodeObject(question, decode);

      expect(test).toHaveProperty('category', decode(question.category));
      expect(test).toHaveProperty('correct_answer', decode(question.correct_answer));
      expect(test).toHaveProperty('difficulty', decode(question.difficulty));
    });
  });
});
