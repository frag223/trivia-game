import * as immutable from '../../app/lib/immutable';

describe('Immutable Library', () => {
  describe('immutablePush()', () => {
    it('it should return a new array', () => {
      const testArray = [1, 2, 3];
      const newArray = immutable.immutablePush(testArray, 'blah');
      expect(testArray).toEqual([1, 2, 3]);
      expect(newArray).toEqual([1, 2, 3, 'blah']);
    });
  });
  describe('randomNumber()', () => {
    it('it should return a number >= 1, <= 3 ', () => {
      const randomNumber = immutable.randomNumber(1, 3);
      expect(randomNumber).toBeLessThanOrEqual(3);
      expect(randomNumber).toBeGreaterThanOrEqual(1);
    });

    it('it should return a number >= 1, <= 2 ', () => {
      const randomNumber = immutable.randomNumber(1, 2);
      expect(randomNumber).toBeLessThanOrEqual(2);
      expect(randomNumber).toBeGreaterThanOrEqual(1);
    });
  });
  describe('unique()', () => {
    it('should return new array of numbers with no duplicates', () => {
      const dupeArray = [1, 1, 2, 3, 3, 3, 3];
      const testArray = immutable.unique(dupeArray);
      expect(testArray).toEqual([1, 2, 3]);
    });

    it('should return new array of strings with no duplicates', () => {
      const dupeArray = ['string', 'hello', 'string'];
      const testArray = immutable.unique(dupeArray);
      expect(testArray).toEqual(['string', 'hello']);
    });
  });
  describe('chunkArray()', () => {
    it('should chunk array of numbers by chunksize 3', () => {
      const originalArray = [1, 2, 3, 4, 5, 6];
      const chunkedArray = immutable.chunkArray(originalArray, 3);
      expect(chunkedArray).toEqual([[1, 2, 3], [4, 5, 6]]);
    });

    it('should chunk array of object by chunksize 3', () => {
      const originalArray = [{ foo: 'bar' }, { bar: 'baz' }, { bin: 'zoo' }];
      const chunkedArray = immutable.chunkArray(originalArray, 2);
      expect(chunkedArray).toEqual([[{ foo: 'bar' }, { bar: 'baz' }], [{ bin: 'zoo' }]]);
    });
  });
});
