import shuffle from '../../app/lib/shuffle';

describe('Shuffle', () => {
  it('should return array in random order', () => {
    const setArray = [1, 2, 3, 4, 5, 6];
    const testArray = shuffle(setArray);
    expect(testArray).not.toEqual(setArray);
  });
});
