import recusive from '../../app/lib/utils/recursive';

describe('recusive()', () => {
  it('should return array value + 222222', () => {
    function testme(value) {
      return value + 222222;
    }
    const test = recusive(testme, ...[['help', 'me'], 'baz']);
    expect(test).toEqual([['help222222', 'me222222'], 'baz222222']);
  });
});
