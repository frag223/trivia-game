import { transformCategory } from '../../app/lib/transform';

describe('normaliseCategories', () => {
  it('nested category should split into category, name, id', () => {
    const params = {
      id: 10,
      name: 'Entertainment: Books',
    };
    const test = transformCategory(params);
    expect(test).toHaveProperty('id', 'category', 'name');
  });
  it('non nested should should split into category: Other, name, id', () => {
    const params = {
      id: 10,
      name: 'Books',
    };
    const test = transformCategory(params);
    expect(test).toHaveProperty('id', 'category', 'name');
  });
});
