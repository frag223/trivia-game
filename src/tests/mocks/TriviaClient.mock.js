
const MockClient = (response, error = false) => {
  const client = () => {
    if (!error) {
      return Promise.resolve(response);
    }
    return Promise.reject(response);
  };
  return {
    triviaClient: client,
  };
};

export default MockClient;
