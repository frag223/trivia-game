import { mockStore } from '../mocks/mockStore';
import updatePlayer from '../../app/actions/player';
import {
  UPDATE_PLAYER_LOADING,
  UPDATE_PLAYER_SUCCESS,
  UPDATE_PLAYER_ERROR,
} from '../../app/config/constants';

import actionFilter from '../testHelpers/actionFilter';

describe('Player Actions', () => {
  describe('Positive Scenarios', () => {
    let store;
    beforeEach(() => {
      store = mockStore();
    });
    afterEach(() => {
      store = '';
    });

    it('updatePlayer should dispatch loading action', () => {
      store.dispatch(updatePlayer('bob'));
      const actions = store.getActions();

      const expectedAction = actionFilter(actions, UPDATE_PLAYER_LOADING);
      expect(expectedAction.length).toBeGreaterThan(0);
      expect(expectedAction[0]).toHaveProperty('loading');
    });

    it('updatePlayer should dispatch success action', () => {
      store.dispatch(updatePlayer('bob'));
      const actions = store.getActions();

      const expectedAction = actionFilter(actions, UPDATE_PLAYER_SUCCESS);
      expect(expectedAction.length).toBeGreaterThan(0);
      expect(expectedAction[0]).toHaveProperty('player');
      expect(expectedAction[0].player).toEqual('bob');
    });
  });
});
