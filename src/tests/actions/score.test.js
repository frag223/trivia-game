import configureMockStore from 'redux-mock-store';
import thunk from 'redux-thunk';
import updateScore from '../../app/actions/score';
import actionFilter from '../testHelpers/actionFilter';
import {
  UPDATE_SCORE_LOADING,
  UPDATE_SCORE_SUCCESS,
} from '../../app/config/constants';

describe('Score Actions', () => {
  describe('Positive Scenarious', () => {
    const mockStore = configureMockStore([thunk]);
    let store;
    beforeEach(() => {
      store = mockStore({
        score: {
          score: 0,
        },
      });
    });

    afterEach(() => {
      store = '';
    });

    it('updateScore() valid params dispatch loading action', () => {
      store.dispatch(updateScore());
      const actions = store.getActions();
      const expectedAction = actionFilter(actions, UPDATE_SCORE_LOADING);
      expect(expectedAction.length).toBeGreaterThan(0);
      expect(expectedAction[0]).toHaveProperty('loading');
    });

    it('updateScore() valid params dispatch success action', () => {
      store.dispatch(updateScore());
      const actions = store.getActions();
      const expectedAction = actionFilter(actions, UPDATE_SCORE_SUCCESS);
      expect(expectedAction.length).toBeGreaterThan(0);
      expect(expectedAction[0]).toHaveProperty('score');
      expect(expectedAction[0].type).toEqual(UPDATE_SCORE_SUCCESS);
    });
  });
});
