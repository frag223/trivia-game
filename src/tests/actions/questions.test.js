import configureMockStore from 'redux-mock-store';
import thunk from 'redux-thunk';
import * as questions from '../../app/actions/questions';
import MockClient from '../mocks/TriviaClient.mock';
import Trivia from '../../app/api/service/trivia';
import validTriviaResponsePayload from '../data/valid/valid-triviaResponsePayload.json';
import actionFilter from '../testHelpers/actionFilter';
import {
  ADD_QUESTIONS_SUCCESS,
  ADD_QUESTIONS_ERROR,
  ADD_QUESTIONS_LOADING,
  UPDATE_QUESTION_SUCCESS,
} from '../../app/config/constants';

describe('Questions Actions', () => {
  describe('Positive Scenarious', () => {
    const client = MockClient(validTriviaResponsePayload, false).triviaClient;
    const trivia = new Trivia(client);
    const params = {
      amount: 5,
      category: 1,
      token: 'abcdef',
    };
    const initialState = {
      questions: [
        {
          id: 1,
          question: 'hello',
        },
      ],
    };

    it('fetchQuestions() with valid params should dispatch success events', () => {
      const mockStore = configureMockStore([thunk.withExtraArgument({ trivia })]);
      const store = mockStore();
      return store.dispatch(questions.fetchQuestions(params))
        .then(() => {
          const actions = store.getActions();
          const expectedAction = actionFilter(actions, ADD_QUESTIONS_SUCCESS);
          expect(expectedAction.length).toBeGreaterThan(0);
          expect(expectedAction[0]).toHaveProperty('questions');
        });
    });
  });

  describe('Negative Scenarios', () => {
    const client = MockClient('expected-error', true).triviaClient;
    const trivia = new Trivia(client);
    const params = {
      amount: 5,
      category: 1,
      token: 'abcdef',
    };

    it('invalid params should dispatch error event', () => {
      const mockStore = configureMockStore([thunk.withExtraArgument({ trivia })]);
      const store = mockStore();
      return store.dispatch(questions.fetchQuestions(params))
        .then(() => {
          const actions = store.getActions();
          const expectedAction = actionFilter(actions, ADD_QUESTIONS_ERROR);
          expect(expectedAction.length).toBeGreaterThan(0);
          expect(expectedAction[0]).toHaveProperty('error');
          expect(expectedAction[0]).toHaveProperty('message');
        });
    });
  });

  describe('updateQuestionsList()', () => {
    it('valid data should dispatch loading and success events', () => {
      const updatedQuestion = [
        {
          id: 'string',
          categoryId: 'string',
          answered: 'boolean',
          category: 'string',
          correct_answer: 'string',
          difficulty: 'string',
          question: 'string',
          incorrect_answers: ['string'],
        },
      ];
      const mockStore = configureMockStore([thunk]);
      const store = mockStore();
      store.dispatch(questions.updateQuestionsList(updatedQuestion));
      const actions = store.getActions();
      const expectedAction = actionFilter(actions, UPDATE_QUESTION_SUCCESS);
      expect(expectedAction.length).toBeGreaterThan(0);
      expect(expectedAction[0]).toHaveProperty('questions');
    });
  });
});
