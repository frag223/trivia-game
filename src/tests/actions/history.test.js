import configureMockStore from 'redux-mock-store';
import thunk from 'redux-thunk';
import updateHistoryList from '../../app/actions/history';
import actionFilter from '../testHelpers/actionFilter';
import {
  ADD_HISTORY_LOADING,
  ADD_HISTORY_ERROR,
  ADD_HISTORY_SUCCESS,
} from '../../app/config/constants';

describe('History Action', () => {
  const mockStore = configureMockStore([thunk]);
  let store;

  const testHistoryList = [
    {
      id: 'string',
      categoryId: 'string',
      answered: false,
      category: 'string',
      correct_answer: 'string',
      difficulty: 'string',
      question: 'string',
      incorrect_answers: ['string', 'string'],
    },
  ];

  beforeEach(() => {
    store = mockStore();
  });

  afterEach(() => {
    store = '';
  });

  it('updateHistoryList(), valid params dispatches loading action', () => {
    store.dispatch(updateHistoryList(testHistoryList));
    const actions = store.getActions();
    const expectedAction = actionFilter(actions, ADD_HISTORY_LOADING);
    expect(expectedAction.length).toBeGreaterThan(0);
    expect(expectedAction[0]).toHaveProperty('loading');
  });

  it('updateHistoryList(), valid params dispatches success action', () => {
    store.dispatch(updateHistoryList(testHistoryList));
    const actions = store.getActions();
    const expectedAction = actionFilter(actions, ADD_HISTORY_SUCCESS);
    expect(expectedAction.length).toBeGreaterThan(0);
    expect(expectedAction[0].history[0]).toHaveProperty('id');
    expect(expectedAction[0].history[0]).toHaveProperty('category');
    expect(expectedAction[0].type).toEqual(ADD_HISTORY_SUCCESS);
  });
});
