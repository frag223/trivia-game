import configureMockStore from 'redux-mock-store';
import thunk from 'redux-thunk';
import addCategories from '../../app/actions/categories';
import MockClient from '../mocks/TriviaClient.mock';
import Trivia from '../../app/api/service/trivia';
import validJsonResponse from '../data/valid/valid-categoiesData.json';
import actionFilter from '../testHelpers/actionFilter';
import {
  ADD_CATEGORIES_SUCCESS,
  ADD_CATEGORIES_ERROR,
  ADD_CATEGORIES_LOADING,
} from '../../app/config/constants';

describe('Categories Actions', () => {
  describe('Positive Scenarios', () => {
    const client = MockClient(validJsonResponse, false).triviaClient;
    const trivia = new Trivia(client);
    let mockStore;
    let store;


    beforeEach(() => {
      mockStore = configureMockStore([thunk.withExtraArgument({ trivia })]);
      store = mockStore();
    });

    afterEach(() => {
      mockStore = '';
      store = '';
    });

    it('addCategories dispatch success event', () => store.dispatch(addCategories())
      .then(() => {
        const actions = store.getActions();
        const expectedAction = actionFilter(actions, ADD_CATEGORIES_SUCCESS);
        expect(expectedAction.length).toBeGreaterThan(0);
        expect(expectedAction[0].type).toEqual(ADD_CATEGORIES_SUCCESS);
        expect(expectedAction[0]).toHaveProperty('categories');
      }));

    it('addCategories dispatch loading event', () => store.dispatch(addCategories())
      .then(() => {
        const actions = store.getActions();
        const expectedAction = actionFilter(actions, ADD_CATEGORIES_LOADING);
        expect(expectedAction.length).toBeGreaterThan(0);
        expect(expectedAction[0].type).toEqual(ADD_CATEGORIES_LOADING);
        expect(expectedAction[0]).toHaveProperty('loading');
      }));
  });

  describe('Negative Scenarios', () => {
    const client = MockClient('expected-error', true).triviaClient;
    const trivia = new Trivia(client);
    let mockStore; let
      store;

    beforeEach(() => {
      mockStore = configureMockStore([thunk.withExtraArgument({ trivia })]);
      store = mockStore();
    });

    afterEach(() => {
      mockStore = '';
      store = '';
    });

    it('Add Categories with invalid data dispatches an Error action', () => store.dispatch(addCategories())
      .then(() => {
        const actions = store.getActions();
        const expectedAction = actionFilter(actions, ADD_CATEGORIES_ERROR);
        expect(expectedAction.length).toBeGreaterThan(0);
        expect(expectedAction[0].type).toEqual(ADD_CATEGORIES_ERROR);
        expect(expectedAction[0]).toHaveProperty('error');
        expect(expectedAction[0]).toHaveProperty('message');
      }));
  });
});
